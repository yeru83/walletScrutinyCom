---
wsId: BitWallet
title: "BitWallet - Buy & Sell Bitcoin"
altTitle: 
authors:
- leo
appId: Tissatech.Bit-Wallet
appCountry: 
idd: 1331439005
released: 2019-02-09
updated: 2021-04-03
version: "1.6.0"
score: 4.85074
reviews: 1005
size: 15589376
developerWebsite: 
repository: 
issue: 
icon: Tissatech.Bit-Wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

