---
wsId: Totalcoin
title: "Bitcoin Wallet App - Totalcoin"
altTitle: 
authors:
- leo
appId: io.totalcoin.wallet
appCountry: 
idd: 1392398906
released: 2018-07-05
updated: 2021-03-17
version: "2.9.7"
score: 4.53846
reviews: 78
size: 56270848
developerWebsite: 
repository: 
issue: 
icon: io.totalcoin.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

