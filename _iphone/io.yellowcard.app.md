---
wsId: yellowcard
title: "Yellow Card: Buy & Sell Crypto"
altTitle: 
authors:
- kiwilamb
- leo
appId: io.yellowcard.app
appCountry: 
idd: 1543252298
released: 2021-03-04
updated: 2021-04-23
version: "1.0.9"
score: 4
reviews: 10
size: 87446528
developerWebsite: https://yellowcard.io
repository: 
issue: 
icon: io.yellowcard.app.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: yellowcard_app
providerLinkedIn: yellowcardapp
providerFacebook: yellowcardapp
providerReddit: 

redirect_from:

---

The Yellow Card wallet has no statements on
[their website](https://yellowcard.io/) or in their FAQ regarding the management
of private keys.
This leads us to conclude the wallet funds are likely under the control of the
provider and hence custodial.

Our verdict: This 'wallet' is **not verifiable**.
