---
wsId: InfinitoWallet
title: "Infinito Wallet - Crypto Safe"
altTitle: 
authors:
- leo
appId: io.infinito.wallet
appCountry: 
idd: 1315572736
released: 2018-01-17
updated: 2021-04-20
version: "2.35.2"
score: 4.24859
reviews: 177
size: 105262080
developerWebsite: 
repository: 
issue: 
icon: io.infinito.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

