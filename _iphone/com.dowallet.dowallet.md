---
wsId: dowallet
title: "DoWallet Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.dowallet.dowallet
appCountry: 
idd: 1451010841
released: 2019-02-03
updated: 2021-04-21
version: "1.1.36"
score: 4.80382
reviews: 209
size: 27224064
developerWebsite: https://www.dowallet.app
repository: 
issue: 
icon: com.dowallet.dowallet.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This wallet sounds like non-custodial. From their description:

> ✓ Simple account creation.
> ✓ Simplified backup and recovery with a 12 word backup phrase.

And from their website:

> We take your security and privacy seriously.
Managing your own private keys is not easy. We are here to help.

Yet we cannot find any link to their source code on Google Play or their website
or doing a [search on GitHub](https://github.com/search?q="com.dowallet").

Our verdict: This wallet is **not verifiable**.
