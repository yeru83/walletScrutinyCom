---
wsId: evercoin
title: "Evercoin: Bitcoin, Ripple, ETH"
altTitle: 
authors:
- leo
appId: com.evercoinInc.evercoin
appCountry: 
idd: 1277924158
released: 2017-09-16
updated: 2020-11-30
version: "1.9.5"
score: 4.65582
reviews: 2766
size: 63333376
developerWebsite: https://evercoin.com
repository: 
issue: 
icon: com.evercoinInc.evercoin.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: everc0in
providerLinkedIn: 
providerFacebook: evercoin
providerReddit: 

redirect_from:

---

This app's description says:

> Evercoin is an integrated non-custodial wallet for managing and exchanging
  cryptocurrencies.

So ... is there source code to reproduce the build?

Unfortunately there is no mention of source code anywhere. Absent source code
this app is **not verifiable**.
