---
wsId: Bexplus
title: "Bexplus- Crypto Margin Trading"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.mhc.bexplus
appCountry: 
idd: 1442189260
released: 2018-11-29
updated: 2021-02-01
version: "2.0.7"
score: 4.81723
reviews: 476
size: 67904512
developerWebsite: https://www.bexplus.com
repository: 
issue: 
icon: com.mhc.bexplus.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-24
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BexplusExchange
providerLinkedIn: 
providerFacebook: 
providerReddit: Bexplus

redirect_from:

---

The Bexplus website states under the mobile wallet section "Assets Security"

> Assets are stored in cold storage against stealing and loss

this leads us to conclude the wallet funds are in control of the provider and
hence custodial.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.
