---
wsId: LumiWallet
title: "Lumi Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.lumiwallet.HD
appCountry: 
idd: 1316477906
released: 2017-12-08
updated: 2021-04-30
version: "3.10.2"
score: 4.82986
reviews: 3356
size: 79925248
developerWebsite: https://lumiwallet.com/
repository: 
issue: 
icon: com.lumiwallet.HD.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

