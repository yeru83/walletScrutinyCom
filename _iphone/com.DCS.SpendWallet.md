---
wsId: spend
title: "Spend App"
altTitle: 
authors:
- kiwilamb
appId: com.DCS.SpendWallet
appCountry: 
idd: 1357740381
released: 2018-03-30
updated: 2020-09-05
version: "3.2.6"
score: 4.55684
reviews: 607
size: 124011520
developerWebsite: https://spend.com
repository: 
issue: 
icon: com.DCS.SpendWallet.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Spendcom
providerLinkedIn: 
providerFacebook: spend
providerReddit: Spend

redirect_from:

---

No statements regarding private key managment can be found on the [providers website](https://www.spend.com/app) or [Support section](https://help.spend.com).
It would be prudent to assume the private keys are under the control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.