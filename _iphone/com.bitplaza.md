---
wsId: bitplaza
title: "Bitplaza - Spend Bitcoin"
altTitle: 
authors:

appId: com.bitplaza
appCountry: 
idd: 1438228771
released: 2018-10-09
updated: 2018-10-09
version: "1.0"
score: 4.3
reviews: 10
size: 32704512
developerWebsite: https://www.bitplazashopping.com/
repository: 
issue: 
icon: com.bitplaza.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This app is a market place with no integrated wallet.
