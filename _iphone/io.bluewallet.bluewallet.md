---
wsId: bluewallet
title: "BlueWallet - Bitcoin wallet"
altTitle: 
authors:
- leo
appId: io.bluewallet.bluewallet
appCountry: 
idd: 1376878040
released: 2018-05-27
updated: 2021-04-06
version: "6.0.8"
score: 4.21171
reviews: 222
size: 66408448
developerWebsite: 
repository: 
issue: 
icon: io.bluewallet.bluewallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

