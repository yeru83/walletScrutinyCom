---
wsId: mona
title: "Crypto.com - Buy Bitcoin Now"
altTitle: 
authors:
- leo
appId: co.mona.Monaco
appCountry: 
idd: 1262148500
released: 2017-08-31
updated: 2021-04-28
version: "3.93"
score: 4.26973
reviews: 15886
size: 266770432
developerWebsite: https://crypto.com/
repository: 
issue: 
icon: co.mona.Monaco.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

As their [version for Android](/android/co.mona.android) this app is custodial
and thus **not verifiable**.
