---
wsId: phemex
title: "Phemex: Buy & Sell Bitcoin"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.phemex.submit
appCountry: 
idd: 1499601684
released: 2020-02-20
updated: 2021-04-12
version: "1.3.2"
score: 4.51804
reviews: 388
size: 139972608
developerWebsite: https://phemex.com/
repository: 
issue: 
icon: com.phemex.submit.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: phemex_official
providerLinkedIn: phemex
providerFacebook: Phemex.official
providerReddit: 

redirect_from:

---

The Phemex mobile app claims to hold funds in cold storage...

> All assets are 100% stored in cold wallets. Each withdrawal is thoroughly
  monitored and requires two-person approval with offline signatures.

leads us to conclude the wallet funds are in control of the provider and hence
custodial.

Our verdict: This 'wallet' is **not verifiable**.
