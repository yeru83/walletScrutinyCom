---
wsId: SwipeWallet
title: "Swipe Wallet"
altTitle: 
authors:
- leo
appId: com.swipe.wallet
appCountry: 
idd: 1476726454
released: 2019-09-10
updated: 2021-03-22
version: "1.532"
score: 4.70851
reviews: 1187
size: 142386176
developerWebsite: https://swipe.io
repository: 
issue: 
icon: com.swipe.wallet.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: SwipeWallet
providerLinkedIn: 
providerFacebook: Swipe
providerReddit: 

redirect_from:

---

This app is a custodial offering:

> SECURE STORAGE<br>
  Swipe users can have peace-of-mind knowing their assets are covered under a
  $100M insurance policy with our custodian. All User deposited funds are stored
  in cold storage with a trusted custodian. Having these funds in a cold storage
  wallet ensures our users that their funds are safe and easily accessible
  through the Swipe Network on the Swipe Wallet.

This contradicts itself. Being in cold storage should mean that it's precisely
not easily accessible via network. What's on the network is by definition a
"hot" wallet.

The website lists "coinbase | custody" and "BitGo" as custodians, which means
you not only have to trust them but also two other services to make sure your
and all the other clients' funds are being accounted for with funds in their
custody.

Anyway, this is all **not verifiable**.
