---
wsId: bybit
title: "Bybit: Bitcoin Crypto Trading"
altTitle: 
authors:
- leo
appId: com.bybit.app
appCountry: 
idd: 1488296980
released: 2020-01-06
updated: 2021-04-21
version: "2.0.5"
score: 3.97525
reviews: 202
size: 111179776
developerWebsite: https://www.bybit.com
repository: 
issue: 
icon: com.bybit.app.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-09
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Bybit_Official
providerLinkedIn: bybitexchange
providerFacebook: Bybit
providerReddit: Bybit

redirect_from:

---

> "Bybit is the World's fastest-growing and one of the largest crypto
  derivatives exchanges to trade Bitcoin and crypto.

and as such, funds are in cold storage with them:

> YOUR SAFETY IS OUR PRIORITY<br>
  We safeguard your cryptocurrencies with a multi-signature cold-wallet
  solution. Your funds are 100% protected from the prying eyes. All traders'
  deposited assets are segregated from Bybit's operating budget to increase our
  financial accountability and transparency.

As a custodial app it is **not verifiable**.
