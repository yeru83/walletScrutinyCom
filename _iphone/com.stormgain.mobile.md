---
wsId: stormgain
title: "StormGain: Handeln mit Krypto"
altTitle: 
authors:
- leo
appId: com.stormgain.mobile
appCountry: de
idd: 1471506070
released: 2019-07-21
updated: 2021-04-28
version: "1.17.0"
score: 4.3788
reviews: 623
size: 54708224
developerWebsite: https://stormgain.com
repository: 
issue: 
icon: com.stormgain.mobile.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: StormGain_com
providerLinkedIn: 
providerFacebook: StormGain.official
providerReddit: 

redirect_from:

---

This app's description mainly focuses on trading and interest earning, features
usually associated with custodial offerings. Also their little paragraph on
security:

> With Industry-Leading Security Protocols, Two-Factor Authentication and Cold
  Wallet Storage, StormGain is a secure environment for Crypto Trading.

sounds more like a custodial offering with the "Cold Wallet Storage" in there.
We conclude the app is **not verifiable**.
