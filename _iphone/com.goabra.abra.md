---
wsId: goabra
title: "Abra: Bitcoin & Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.goabra.abra
appCountry: 
idd: 966301394
released: 2015-03-12
updated: 2021-04-05
version: "95.0.0"
score: 4.5609
reviews: 14525
size: 103822336
developerWebsite: 
repository: 
issue: 
icon: com.goabra.abra.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AbraGlobal
providerLinkedIn: abra
providerFacebook: GoAbraGlobal
providerReddit: 

redirect_from:

---

This is the iPhone version of the
[Android Abra Bitcoin Crypto Wallet Buy Trade Earn Interest](/android/com.plutus.wallet).

Just like the Android version, this wallet is **not verifiable**.
