---
wsId: Luno
title: "Luno Bitcoin & Cryptocurrency"
altTitle: 
authors:
- leo
appId: za.co.Bitx
appCountry: 
idd: 927362479
released: 2014-11-03
updated: 2021-04-19
version: "7.12.0"
score: 4.43821
reviews: 3245
size: 89352192
developerWebsite: https://www.luno.com
repository: 
issue: 
icon: za.co.Bitx.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

