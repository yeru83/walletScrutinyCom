---
wsId: zeusln
title: "Zeus LN"
altTitle: 
authors:

appId: com.zeusln.zeus
appCountry: 
idd: 1456038895
released: 2021-04-22
updated: 2021-04-23
version: "0.5.1"
score: 5
reviews: 6
size: 25418752
developerWebsite: https://ZeusLN.app
repository: 
issue: 
icon: com.zeusln.zeus.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-27
reviewStale: false
signer: 
reviewArchive:


providerTwitter: ZeusLN
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This app is a bit special as it does not hold your private keys but neither is
it custodial. It remote-controls your lightning node that you can run for
example at home. So it is a wallet in that you can use it to send and receive
Bitcoins.

And ... best of all:

> Furthermore our builds have no proprietary dependencies, are reproducible, and
  are distributed on F-Droid.

This only applies for the Android version of course and to see how that went,
check out the Android app. For iPhone app, reproducibility is still an
unresolved issue which leaves this app to be open source but still
**not verifiable**.
