---
wsId: mw.org.freewallet
title: "Multi Crypto Wallet－Freewallet"
altTitle: 
authors:
- leo
appId: mw.org.freewallet.app
appCountry: 
idd: 1274003898
released: 2017-09-01
updated: 2021-01-19
version: "1.15.0"
score: 3.8972
reviews: 963
size: 45145088
developerWebsite: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: 

redirect_from:

---

According to the description

> In addition, the majority of cryptocurrency assets on the platform are stored
  in an offline vault. Your coins will be kept in cold storage with state of the
  art security protecting them.

This is a custodial app.

Our verdict: **not verifiable**.
