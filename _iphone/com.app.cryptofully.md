---
wsId: cryptofully
title: "Cryptofully"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.app.cryptofully
appCountry: 
idd: 1533929589
released: 2020-11-15
updated: 2021-03-17
version: "1.1.9"
score: 4.58621
reviews: 29
size: 43956224
developerWebsite: https://www.cryptofully.com
repository: 
issue: 
icon: com.app.cryptofully.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-18
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptofully
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This mobile app is an exchange solution aimed at transferring money into Nigerian bank accounts.
The user can use other Bitcoin wallets to send BTC to receive addresses in the
app to initiate deposits to Nigerian Bank accounts.

It is not designed to store BTC, thus is **not a wallet**.
