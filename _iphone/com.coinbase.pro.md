---
wsId: coinbasepro
title: "Coinbase Pro"
altTitle: 
authors:
- leo
appId: com.coinbase.pro
appCountry: 
idd: 1446636681
released: 2019-10-10
updated: 2021-04-26
version: "1.0.73"
score: 4.7262
reviews: 68925
size: 46872576
developerWebsite: https://pro.coinbase.com
repository: 
issue: 
icon: com.coinbase.pro.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinbasePro
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:

---

On the website we read:

> **Insurance protection**<br>
  All digital assets held in online storage are fully insured. All USD balances
  are covered by FDIC insurance, up to a maximum of $250,000 per customer.

which look purely custodial. This app is **not verifiable**.
