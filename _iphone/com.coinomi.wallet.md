---
wsId: coinomi
title: "Coinomi Wallet"
altTitle: 
authors:
- leo
appId: com.coinomi.wallet
appCountry: 
idd: 1333588809
released: 2018-03-22
updated: 2021-04-16
version: "1.9.4"
score: 4.51999
reviews: 1025
size: 116429824
developerWebsite: https://www.coinomi.com
repository: 
issue: 
icon: com.coinomi.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

