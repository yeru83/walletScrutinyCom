---
wsId: xapo
title: "Xapo"
altTitle: 
authors:
- leo
appId: com.iphone.XapoApp
appCountry: 
idd: 917692892
released: 2014-11-13
updated: 2021-04-24
version: "6.11.3"
score: 3.95775
reviews: 142
size: 151600128
developerWebsite: https://xapo.com
repository: 
issue: 
icon: com.iphone.XapoApp.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

