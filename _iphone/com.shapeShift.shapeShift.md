---
wsId: ShapeShift
title: "ShapeShift: Buy & Trade Crypto"
altTitle: 
authors:
- leo
appId: com.shapeShift.shapeShift
appCountry: 
idd: 996569075
released: 2015-06-09
updated: 2021-04-16
version: "2.14.0"
score: 2.87133
reviews: 443
size: 78240768
developerWebsite: https://shapeshift.com
repository: 
issue: 
icon: com.shapeShift.shapeShift.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

