---
wsId: jaxxliberty
title: "Jaxx Liberty Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.liberty.jaxx
appCountry: 
idd: 1435383184
released: 2018-10-03
updated: 2021-04-20
version: "2.6.3"
score: 4.51143
reviews: 1269
size: 43938816
developerWebsite: https://jaxx.io
repository: 
issue: 
icon: com.liberty.jaxx.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

