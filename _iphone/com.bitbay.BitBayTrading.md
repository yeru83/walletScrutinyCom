---
wsId: bitpaytrading
title: "BitBay - Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: com.bitbay.BitBayTrading
appCountry: 
idd: 1409644952
released: 2018-11-20
updated: 2021-04-02
version: "1.3.21"
score: 3.5
reviews: 20
size: 99395584
developerWebsite: https://bitbay.net
repository: 
issue: 
icon: com.bitbay.BitBayTrading.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

