---
wsId: bitpaywallet
title: "BitPay – Buy Crypto"
altTitle: 
authors:
- leo
appId: com.bitpay.wallet
appCountry: 
idd: 1149581638
released: 2016-10-24
updated: 2021-04-23
version: "12.4.4"
score: 3.98854
reviews: 1134
size: 87089152
developerWebsite: https://bitpay.com
repository: 
issue: 
icon: com.bitpay.wallet.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-27
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BitPay
providerLinkedIn: bitpay-inc-
providerFacebook: BitPayOfficial
providerReddit: 

redirect_from:

---

BitPay – Secure Bitcoin Wallet links to its source code on their App Store
description.

As reproducible builds are a general problem with how the App Store works,
this app just like its Android version is **not verifiable**.

See the Android version to get an idea of how (not so) easy it is to build the
app from the source code.
