---
wsId: coinpayapp
title: "Coin Pay Wallet: Buy Bitcoin"
altTitle: 
authors:
- leo
appId: com.coinpayapp.CoinPay
appCountry: 
idd: 1477731032
released: 2019-12-04
updated: 2020-08-29
version: "2020.08.28"
score: 4.03092
reviews: 97
size: 21357568
developerWebsite: https://www.coinpayapp.com
repository: 
issue: 
icon: com.coinpayapp.CoinPay.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

