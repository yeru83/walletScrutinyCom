---
wsId: CoinZoom
title: "CoinZoom Pro"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.ios.coinzoom
appCountry: 
idd: 1443109132
released: 2020-03-05
updated: 2021-04-15
version: "1.1.27"
score: 4.77995
reviews: 409
size: 78922752
developerWebsite: http://www.coinzoom.com
repository: 
issue: 
icon: com.ios.coinzoom.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: GetCoinZoom
providerLinkedIn: coinzoomhq
providerFacebook: CoinZoom
providerReddit: 

redirect_from:

---

The CoinZoom [support FAQ](https://www.coinzoom.com/support/) states the wallet
is custodial... 

Found under "Where can I find the private keys for my wallet?":

> As CoinZoom is a hosted wallet, it's not feasible to provide the private keys
  to individual wallet addresses; doing so would prevent us from taking
  advantage of our secure cold-storage technology to protect your funds.

this leads us to conclude the wallet funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.
