---
wsId: tabtrader
title: "TabTrader"
altTitle: 
authors:
- leo
- kiwilamb
appId: com.tabtrader.apps.TabTrader
appCountry: 
idd: 1095716562
released: 2016-09-02
updated: 2021-02-26
version: "2.9.1"
score: 4.74404
reviews: 3942
size: 23987200
developerWebsite: https://tab-trader.com
repository: 
issue: 
icon: com.tabtrader.apps.TabTrader.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: tabtraderpro
providerLinkedIn: tabtrader
providerFacebook: tabtrader
providerReddit: 

redirect_from:

---

This app appears to not function as a wallet. At least we could not see any
documentation about depositing or withdrawing through the app, which makes the
verdict **not a wallet** but the app still has still massive potential for abuse
if the provider front-runs the trades of the users from the insight they gain or
even worse, they could trigger lucrative-to-front-run trades the user never
intended to make.
