---
wsId: LiteBit
title: "LiteBit - Buy & sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.litebit.app
appCountry: 
idd: 1448841440
released: 2019-08-20
updated: 2021-04-22
version: "3.0.6"
score: 4
reviews: 8
size: 64972800
developerWebsite: https://www.litebit.eu/en/
repository: 
issue: 
icon: com.litebit.app.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

