---
wsId: cex
title: "CEX.IO Cryptocurrency Exchange"
altTitle: 
authors:
- kiwilamb
- leo
appId: io.cex.app
appCountry: 
idd: 1047225016
released: 2015-12-22
updated: 2021-03-24
version: "6.9.2"
score: 4.59739
reviews: 3599
size: 82926592
developerWebsite: https://cex.io
repository: 
issue: 
icon: io.cex.app.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cex_io
providerLinkedIn: cex-io
providerFacebook: CEX.IO
providerReddit: 

redirect_from:

---

The CEX.io mobile app claims on the website to manage bitcoins...

> Stay in control of your funds anywhere. Deposit and withdraw crypto and fiat, add your debit or credit card in a few clicks, and store your funds securely.

however their is no evidence of the wallet being non-custodial, with no source code repository listed or found...

Our verdict: This 'wallet' is probably custodial but does not provide public source and therefore is **not verifiable**.
