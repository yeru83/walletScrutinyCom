---
wsId: coinbaseBSB
title: "Coinbase – Buy & sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.vilcsak.bitcoin2
appCountry: 
idd: 886427730
released: 2014-06-22
updated: 2021-04-26
version: "9.21.3"
score: 4.68499
reviews: 1163279
size: 75065344
developerWebsite: http://www.coinbase.com
repository: 
issue: 
icon: com.vilcsak.bitcoin2.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinbase
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:

---

This app's provider claims:

> Over 98% of cryptocurrency is stored securely offline and the rest is
  protected by industry-leading online security.

which clearly means it is a custodial offering.
