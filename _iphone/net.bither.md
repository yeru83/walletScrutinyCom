---
wsId: bither
title: "Bither - Bitcoin Wallet"
altTitle: 
authors:

appId: net.bither
appCountry: 
idd: 899478936
released: 2014-07-25
updated: 2021-02-12
version: "2.0.0"
score: 2.89744
reviews: 39
size: 16953344
developerWebsite: https://bither.net
repository: https://github.com/bither/bither-ios
issue: 
icon: net.bither.jpg
bugbounty: 
verdict: obfuscated # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

At the risk of putting it into the wrong **not verifiable** category, we assume
the App Store version is no better and refer to
[the Play Store review](/android/net.bither) for our analysis.
