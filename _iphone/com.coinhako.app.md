---
wsId: 
title: "Coinhako: Bitcoin Wallet Asia"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.coinhako.app
appCountry: 
idd: 1137855704
released: 2016-09-04
updated: 2021-04-08
version: "3.2.6"
score: 3.33333
reviews: 9
size: 57584640
developerWebsite: https://www.coinhako.com
repository: 
issue: 
icon: com.coinhako.app.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-24
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinhako
providerLinkedIn: coinhako
providerFacebook: coinhako
providerReddit: 

redirect_from:

---

Having a scan over the providers website and faq articles does not reveal any
claims regarding the management of private keys.
We would have to assume this wallet is custodial.

Our verdict: This “wallet” is probably custodial and therefore is **not verifiable**.
