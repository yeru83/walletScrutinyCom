---
wsId: WalletsAfrica
title: "Wallets Africa"
altTitle: 
authors:
- kiwilamb
- leo
appId: ng.wallet.prod
appCountry: 
idd: 1280830303
released: 2017-10-14
updated: 2021-05-01
version: "2.472"
score: 4.42373
reviews: 59
size: 59242496
developerWebsite: http://wallets.africa
repository: 
issue: 
icon: ng.wallet.prod.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-24
reviewStale: true
signer: 
reviewArchive:


providerTwitter: walletsafrica
providerLinkedIn: 
providerFacebook: walletsafrica
providerReddit: 

redirect_from:

---

Wallets Africa is quite a broad product, the lack of source code makes it
impossible to verify this app and there are no statements on their website as to
management of private keys.

Our verdict: This “wallet” is probably custodial and therefore is
**not verifiable**.
