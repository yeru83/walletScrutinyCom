---
wsId: valr
title: "VALR Bitcoin Exchange & Wallet"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.valr.mobile
appCountry: 
idd: 1453499428
released: 2019-09-27
updated: 2021-04-20
version: "1.0.25"
score: 5
reviews: 6
size: 46091264
developerWebsite: https://www.valr.com
repository: 
issue: 
icon: com.valr.mobile.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: valrdotcom
providerLinkedIn: valr
providerFacebook: VALRdotcom
providerReddit: 

redirect_from:

---

I need not go further into researching this wallet as the statement on the
Google Play description screams custodial.

> We hold your cryptocurrencies in both “cold storage” and “hot wallets”.

This is an exchange trading wallet that holds the customers funds in the
providers control.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
