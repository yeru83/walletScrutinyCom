---
wsId: midas
title: "Midas Protocol - Crypto Wallet"
altTitle: 
authors:
- kiwilamb
appId: com.midasprotocol.wallet.ios
appCountry: 
idd: 1436698193
released: 2018-09-24
updated: 2021-04-22
version: "2.0"
score: 4.85714
reviews: 70
size: 146430976
developerWebsite: https://midasprotocol.io/
repository: 
issue: 
icon: com.midasprotocol.wallet.ios.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: MidasProtocol
providerLinkedIn: 
providerFacebook: midasprotocol.io
providerReddit: 

redirect_from:

---

No statements regarding private key managment can be found on the [providers website](https://midasprotocol.io/) or [Support section](https://support.midasprotocol.io/hc/en-us).
It would be prudent to assume the private keys are under the control of the provider.


Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.