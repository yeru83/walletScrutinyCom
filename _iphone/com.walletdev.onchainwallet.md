---
wsId: HuobiWallet
title: "Huobi Wallet - Safe & Reliable"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.walletdev.onchainwallet
appCountry: 
idd: 1433883012
released: 2018-09-29
updated: 2021-04-17
version: "2.7.3"
score: 4.63441
reviews: 279
size: 87490560
developerWebsite: https://www.huobiwallet.com/en/
repository: 
issue: 
icon: com.walletdev.onchainwallet.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: HuobiWallet
providerLinkedIn: 
providerFacebook: HuobiWallet
providerReddit: 

redirect_from:

---

From the description on the App Store the wallet provider clearly states the
private keys are in control of the user:

> Huobi Wallet users have sole control over their own private keys and thus have
  full control over their assets. There are no third parties involved in
  management of private keys.

However the non-custodial claims of the provider cannot be verified as no source
code is available.

Our verdict: This 'wallet' is possibly non-custodial but does not provide public
source and therefore is **not verifiable**.
