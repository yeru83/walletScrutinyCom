---
wsId: SpotWalletapp
title: "Buy Bitcoin - Spot Wallet app"
altTitle: 
authors:
- leo
appId: tech.spotapp.spot
appCountry: 
idd: 1390560448
released: 2018-08-07
updated: 2021-04-22
version: "3.2.5"
score: 4.61883
reviews: 3686
size: 84225024
developerWebsite: https://spot-bitcoin.com
repository: 
issue: 
icon: tech.spotapp.spot.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

