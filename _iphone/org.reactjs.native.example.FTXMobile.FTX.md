---
wsId: FTXPro
title: "FTX Pro: Trade Crypto Anywhere"
altTitle: 
authors:
- leo
appId: org.reactjs.native.example.FTXMobile.FTX
appCountry: 
idd: 1512374471
released: 2020-05-20
updated: 2021-01-21
version: "1.1.0"
score: 4.36735
reviews: 49
size: 27563008
developerWebsite: https://ftx.com
repository: 
issue: 
icon: org.reactjs.native.example.FTXMobile.FTX.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-08
reviewStale: false
signer: 
reviewArchive:


providerTwitter: FTX_Official
providerLinkedIn: 
providerFacebook: ftx.official
providerReddit: 

redirect_from:

---

> Security is a critical concern for a cryptocurrency exchange. FTX uses
  industry-leading security solutions built in-house (both online and offline).

"industry-leading" and "built in-house" found in the same sentence is kind of
scary when it doesn't come from the actual industry leader. Either way, the
"online and offline" part sounds very custodial and thus **not verifiable**.
