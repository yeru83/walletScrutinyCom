---
wsId: casaapp
title: "Casa App - Secure your Bitcoin"
altTitle: 
authors:
- leo
appId: com.casa.vault
appCountry: 
idd: 1314586706
released: 2018-08-02
updated: 2021-04-30
version: "3.0.3"
score: 4.92253
reviews: 284
size: 45547520
developerWebsite: https://keys.casa
repository: 
issue: 
icon: com.casa.vault.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

