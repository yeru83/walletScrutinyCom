---
wsId: AirGapVault
title: "AirGap Vault - Secure Secrets"
altTitle: 
authors:
- leo
appId: it.airgap.vault
appCountry: 
idd: 1417126841
released: 2018-08-24
updated: 2021-04-26
version: "3.7.0"
score: 4.33333
reviews: 3
size: 87072768
developerWebsite: 
repository: 
issue: 
icon: it.airgap.vault.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:

---

