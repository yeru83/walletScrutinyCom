---
wsId: flits
title: "Flits - Passive income wallet"
altTitle: 
authors:
- leo
appId: app.flitsnode.flits
appCountry: 
idd: 1460563713
released: 2019-04-30
updated: 2021-02-09
version: "4.4"
score: 4.27869
reviews: 61
size: 82679808
developerWebsite: https://flitsnode.app
repository: 
issue: 
icon: app.flitsnode.flits.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: FlitsNode
providerLinkedIn: 
providerFacebook: flitsnode
providerReddit: 

redirect_from:

---

This app appears to support Bitcoin:

> Use Bitcoin or Etherium to get started and deploy a masternode in seconds with
  the Flits app!

and self-custodial:

> You are the only one who controls the keys! Your crypto funds are 100% safely
  stored inside your phone!

but can we verify those claims?

On their website they brag with:

> **311 BTC**<br>
  Total wallet value

so they have quite some insight into their users' financial activity. Certainly
not great if you want more privacy. Together with the other claim:

> **33706**<br>
  Users

we get to an average balance of 9mBTC or $440US.

What we can not find though is their source code, so the app is **not verifiable**.
