---
wsId: cobowallet
title: "Cobo Crypto Wallet: BTC & DASH"
altTitle: 
authors:
- leo
appId: cobo.wallet
appCountry: 
idd: 1406282615
released: 2018-08-05
updated: 2021-04-28
version: "5.0.2"
score: 
reviews: 
size: 116819968
developerWebsite: https://cobo.com
repository: 
issue: 
icon: cobo.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

