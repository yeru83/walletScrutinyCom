---
wsId: WalletofSatoshi
title: "Wallet of Satoshi"
altTitle: 
authors:
- leo
appId: com.livingroomofsatoshi.wallet
appCountry: 
idd: 1438599608
released: 2019-05-20
updated: 2021-04-08
version: "1.10.7"
score: 3.42857
reviews: 28
size: 33015808
developerWebsite: https://www.walletofsatoshi.com
repository: 
issue: 
icon: com.livingroomofsatoshi.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

