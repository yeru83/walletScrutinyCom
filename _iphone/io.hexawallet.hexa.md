---
wsId: Hexa
title: "Bitcoin Wallet Hexa"
altTitle: 
authors:
- leo
appId: io.hexawallet.hexa
appCountry: 
idd: 1490205837
released: 2020-03-16
updated: 2021-05-02
version: "1.6.0"
score: 4.8
reviews: 5
size: 51562496
developerWebsite: https://hexawallet.io/
repository: 
issue: 
icon: io.hexawallet.hexa.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

