---
wsId: ChangeNOW
title: "ChangeNOW Crypto Exchange"
altTitle: 
authors:
- leo
appId: io.changenow
appCountry: 
idd: 1518003605
released: 2020-06-29
updated: 2021-04-18
version: "1.4.4"
score: 4.3287
reviews: 216
size: 31773696
developerWebsite: 
repository: 
issue: 
icon: io.changenow.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

