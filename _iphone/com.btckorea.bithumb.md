---
wsId: bithumbko
title: "Bithumb"
altTitle: 
authors:
- leo
appId: com.btckorea.bithumb
appCountry: 
idd: 1299421592
released: 2017-12-05
updated: 2021-04-23
version: "1.5.2"
score: 1.9
reviews: 20
size: 77831168
developerWebsite: https://en.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

