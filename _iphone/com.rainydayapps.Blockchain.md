---
wsId: blockchainWallet
title: "Blockchain Wallet: Buy Bitcoin"
altTitle: 
authors:
- leo
appId: com.rainydayapps.Blockchain
appCountry: 
idd: 493253309
released: 2012-04-13
updated: 2021-04-12
version: "3.3.2"
score: 4.73036
reviews: 68594
size: 96881664
developerWebsite: https://www.blockchain.com/wallet
repository: https://github.com/blockchain/My-Wallet-V3-iOS
issue: 
icon: com.rainydayapps.Blockchain.jpg
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Blockchain
providerLinkedIn: blockchain
providerFacebook: Blockchain
providerReddit: 

redirect_from:

---

On the App Store we read:

> Only you have access to your private keys and your crypto.

and on the website there is a link to
[this GitHub account](https://github.com/blockchain/) but no claim about which
repository is supposed to be behind this wallet but we assume it's
[this](https://github.com/blockchain/My-Wallet-V3-iOS).

As with all iPhone app, reproducible builds do not exist so far, so the app
is **not verifiable**.
