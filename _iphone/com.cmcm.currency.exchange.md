---
wsId: bitrue
title: "Bitrue"
altTitle: 
authors:
- leo
appId: com.cmcm.currency.exchange
appCountry: 
idd: 1435877386
released: 2018-09-16
updated: 2021-04-30
version: "4.4.2"
score: 3.0979
reviews: 143
size: 91182080
developerWebsite: 
repository: 
issue: 
icon: com.cmcm.currency.exchange.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

