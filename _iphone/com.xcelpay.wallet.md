---
wsId: XcelPay
title: "XcelPay - Secure Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.xcelpay.wallet
appCountry: 
idd: 1461215417
released: 2019-05-26
updated: 2021-05-01
version: "2.20.6"
score: 4.05
reviews: 20
size: 40544256
developerWebsite: http://xcelpay.io
repository: 
issue: 
icon: com.xcelpay.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

