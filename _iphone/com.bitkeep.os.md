---
wsId: bitkeep
title: "BitKeep"
altTitle: 
authors:
- leo
appId: com.bitkeep.os
appCountry: 
idd: 1395301115
released: 2018-09-26
updated: 2021-03-23
version: "5.1.2"
score: 3
reviews: 6
size: 62444544
developerWebsite: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

