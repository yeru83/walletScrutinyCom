---
wsId: AirGapWallet
title: "AirGap Wallet"
altTitle: 
authors:

appId: it.airgap.wallet
appCountry: 
idd: 1420996542
released: 2018-08-24
updated: 2021-04-28
version: "3.7.1"
score: 3.33333
reviews: 6
size: 100145152
developerWebsite: 
repository: 
issue: 
icon: it.airgap.wallet.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:

---

This is the iPhone version of [this Android app](/android/it.airgap.wallet/).
