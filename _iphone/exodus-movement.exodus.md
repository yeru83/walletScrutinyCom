---
wsId: ExodusCryptoBitcoinWallet
title: "Exodus: Crypto Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: exodus-movement.exodus
appCountry: 
idd: 1414384820
released: 2019-03-23
updated: 2021-04-23
version: "21.4.22"
score: 4.58595
reviews: 10431
size: 29526016
developerWebsite: https://exodus.com/mobile
repository: 
issue: 
icon: exodus-movement.exodus.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: exodus_io
providerLinkedIn: 
providerFacebook: exodus.io
providerReddit: 

redirect_from:

---

Just like [their Android wallet](/android/exodusmovement.exodus/), this app is
closed source and thus **not verifiable**.
