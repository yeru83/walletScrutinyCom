---
wsId: etoro
title: "eToro Cryptocurrency Trading"
altTitle: 
authors:
- leo
appId: com.etoro.openbook
appCountry: 
idd: 674984916
released: 2017-06-26
updated: 2021-04-29
version: "319.0.0"
score: 4.05736
reviews: 2824
size: 131479552
developerWebsite: http://www.etoro.com
repository: 
issue: 
icon: com.etoro.openbook.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: etoro
providerLinkedIn: etoro
providerFacebook: eToro
providerReddit: 

redirect_from:

---

Etoro is used to speculate on assets more than to actually transfer them but in
the case of Bitcoin, according to
[the Help Center](https://www.etoro.com/customer-service/help/1422157482/can-i-withdraw-my-cryptocurrencies-from-the-platform/)
you can actually send Bitcoins from this app ... if you are in the right
jurisdiction ...
[further restrictions apply](https://etoro.nanorep.co/widget/widget.html?kb=156763&account=etoro#onloadquestionid=1306615492) ...

So all in all this could pass as a custodial app.

As a custodial app it is **not verifiable**.
