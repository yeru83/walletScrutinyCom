---
wsId: BinanceUS
title: "Binance.US - Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: us.binance.fiat
appCountry: 
idd: 1492670702
released: 2020-01-05
updated: 2021-04-26
version: "2.4.3"
score: 4.17275
reviews: 45089
size: 100300800
developerWebsite: https://www.binance.us
repository: 
issue: 
icon: us.binance.fiat.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: binance-us
providerFacebook: BinanceUS
providerReddit: 

redirect_from:

---

This is the iPhone version of [this Android app](/android/com.binance.us) and we
come to the same conclusion for the same reasons. This app is **not verifiable**.
