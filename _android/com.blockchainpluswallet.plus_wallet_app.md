---
wsId: 
title: "Blockchain Plus Wallet"
altTitle: 
authors:

users: 5000
appId: com.blockchainpluswallet.plus_wallet_app
launchDate: 
latestUpdate: 2020-12-30
apkVersionName: "3.0.1"
stars: 3.6
ratings: 191
reviews: 148
size: 13M
website: 
repository: 
issue: 
icon: com.blockchainpluswallet.plus_wallet_app.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.blockchainpluswallet.plus_wallet_app/
---


This app does not look like a wallet and its code does not look like having
wallet functionality neither. It might side-load some actual functionality at
some point via the used Flutter Webview but given it has almost only 5* fake
reviews and the welcome screen only asks "enter indroduction or restore here"
without allowing the user to create a wallet, we have to assume there is
something very shady going on and **recommend to not use this app.**
