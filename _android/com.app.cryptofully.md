---
wsId: cryptofully
title: "Cryptofully"
altTitle: 
authors:
- kiwilamb
- leo
users: 5000
appId: com.app.cryptofully
launchDate: 
latestUpdate: 2021-03-17
apkVersionName: "1.1.9"
stars: 4.2
ratings: 275
reviews: 112
size: 29M
website: https://www.cryptofully.com/
repository: 
issue: 
icon: com.app.cryptofully.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: cryptofully
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This mobile app is an exchange solution aimed at transferring money into Nigerian bank accounts.
The user can use other Bitcoin wallets to send BTC to receive addresses in the
app to initiate deposits to Nigerian Bank accounts.

It is not designed to store BTC, thus is **not a wallet**.
