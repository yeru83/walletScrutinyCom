---
wsId: TECHBANK
title: "TechBank"
altTitle: 
authors:
- leo
users: 10000
appId: com.beeone.techbank
launchDate: 
latestUpdate: 2021-04-30
apkVersionName: "4.5"
stars: 3.9
ratings: 555
reviews: 235
size: 42M
website: https://techbank.finance
repository: 
issue: 
icon: com.beeone.techbank.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.beeone.techbank/
---


Neither on Google Play nor their website do we found claims about this app being
a non-custodial wallet and as the name Tech**Bank** sounds rather custodial, we
file it as such and conclude this app is **not verifiable**.
