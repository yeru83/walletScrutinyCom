---
wsId: exmo
title: "EXMO: Buy & Sell Bitcoin (BTC) on Crypto Exchange"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.exmo
launchDate: 
latestUpdate: 2021-04-27
apkVersionName: "2.1.1"
stars: 4.5
ratings: 1679
reviews: 1155
size: 55M
website: https://exmo.com
repository: 
issue: 
icon: com.exmo.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-21
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Exmo_com
providerLinkedIn: 
providerFacebook: exmo.market
providerReddit: 

redirect_from:

---


The Exmo [support FAQ](https://info.exmo.com/en/faq/) states under "Where are my EXMO funds kept?"

> Users cryptocurrency funds are stored on the exchange’s crypto wallets: cold and hot vaults.

this leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
