---
wsId: strike
title: "Strike: Bitcoin & Lightning Payments"
altTitle: 
authors:

users: 50000
appId: zapsolutions.strike
launchDate: 
latestUpdate: 2021-05-01
apkVersionName: "v72"
stars: 4.3
ratings: 1406
reviews: 491
size: 9.0M
website: 
repository: 
issue: 
icon: zapsolutions.strike.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-18
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ln_strike
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /zapsolutions.strike/
---


This app does not hold Bitcoins but it allows to interact with Bitcoin wallets
in that you can send to and receive from Bitcoin wallets but your balance is
always in fiat.

The provider claims this wallet is non-custodial but ...

> **Is Strike custodial?**<br>
  No. Users never custody Bitcoin through Strike. Strike is the custodian of the
  fiat deposited onto the app. All fiat is FDIC-insured and users are free to
  withdraw these funds to their linked account at any time.

that sounds like a "custodial wallet", doesn't it? It's **not a Bitcoin wallet**
though. We could also go with "custodial" as all the
funds are being held by the provider and it sure enough is promoted as a Bitcoin
wallet. Please raise an issue
[on our GitLab](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/new)
if you feel strong about this verdict.
