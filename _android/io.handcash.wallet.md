---
wsId: 
title: "HandCash"
altTitle: 
authors:

users: 50000
appId: io.handcash.wallet
launchDate: 2019-09-10
latestUpdate: 2021-04-17
apkVersionName: "2.6.3"
stars: 4.3
ratings: 411
reviews: 270
size: 35M
website: https://handcash.io
repository: 
issue: 
icon: io.handcash.wallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: handcashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.handcash.wallet/
  - /posts/io.handcash.wallet/
---


A BSV wallet.
