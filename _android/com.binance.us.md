---
wsId: BinanceUS
title: "Binance.US"
altTitle: 
authors:
- leo
users: 500000
appId: com.binance.us
launchDate: 
latestUpdate: 2021-04-25
apkVersionName: "2.4.3"
stars: 1.8
ratings: 3437
reviews: 2650
size: Varies with device
website: https://www.binance.us
repository: 
issue: 
icon: com.binance.us.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: binance-us
providerFacebook: BinanceUS
providerReddit: 

redirect_from:
  - /com.binance.us/
---


Binance being a big exchange, the description on Google Play only mentions
security features like FDIC insurance for USD balance but no word on
self-custody. Their website is not providing more information neither. We
assume the app is a custodial offering and therefore **not verifiable**.
