---
wsId: krcokeypair
title: "NExTouch"
altTitle: 
authors:

users: 1000
appId: kr.co.keypair.nextouch
launchDate: 
latestUpdate: 2019-07-22
apkVersionName: "1.0.0.57"
stars: 3.5
ratings: 8
reviews: 5
size: 12M
website: http://www.eunex.co
repository: 
issue: 
icon: kr.co.keypair.nextouch.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


As far as we can see, this is the same as
[this app](/android/kr.co.keypair.keywalletTouch) and thus is **not verifiable**.
