---
wsId: dowallet
title: "DoWallet: Bitcoin Wallet. A Secure Crypto Wallet."
altTitle: 
authors:
- leo
users: 50000
appId: com.dowallet
launchDate: 2019-01-01
latestUpdate: 2021-04-20
apkVersionName: "1.1.36"
stars: 3.8
ratings: 834
reviews: 445
size: 33M
website: https://www.dowallet.app
repository: 
issue: 
icon: com.dowallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-11-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This wallet sounds like non-custodial. From their description:

> ✓ Simple account creation.
> ✓ Simplified backup and recovery with a 12 word backup phrase.

And from their website:

> We take your security and privacy seriously.
Managing your own private keys is not easy. We are here to help.

Yet we cannot find any link to their source code on Google Play or their website
or doing a [search on GitHub](https://github.com/search?q="com.dowallet").

Our verdict: This wallet is **not verifiable**.
