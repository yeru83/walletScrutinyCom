---
wsId: coincola
title: "CoinCola - Buy Bitcoin & more"
altTitle: 
authors:
- leo
users: 50000
appId: com.newgo.coincola
launchDate: 
latestUpdate: 2021-03-04
apkVersionName: "4.7.2"
stars: 3.3
ratings: 593
reviews: 276
size: 31M
website: https://www.coincola.com
repository: 
issue: 
icon: com.newgo.coincola.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-03
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinCola_Global
providerLinkedIn: coincola
providerFacebook: CoinCola
providerReddit: coincolaofficial

redirect_from:
  - /com.newgo.coincola/
---


> SAFE AND SECURE<br>
> Our team uses bank-level encryption, cold storage and SSL for the highest level of security.

Cold storage has only a meaning in the context of a custodial app. As such it
is **not verifiable**.
