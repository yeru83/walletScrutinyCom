---
wsId: safepal
title: "SafePal - Crypto wallet BTC ETH LTC BNB Tron EOS"
altTitle: 
authors:
- leo
users: 100000
appId: io.safepal.wallet
launchDate: 
latestUpdate: 2021-04-28
apkVersionName: "2.5.10"
stars: 4.1
ratings: 5558
reviews: 3235
size: 30M
website: https://www.safepal.io
repository: 
issue: 
icon: io.safepal.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: iSafePal
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-04-26**: Now some months after the original review, Google finds
some things that confirms the claims of Binance "investing" in SafePal. The best
link is probably
[this article on SafePal by Binance](https://research.binance.com/en/projects/safepal).

The app being closed source remains an issue:

> SafePal cryptocurrency wallet application is a decentralized application.
  The mnemonic phrase is stored by users. SafePal does not read or store
  sensitive account information including private key and mnemonic phrase.

So they do claim to be non-custodial but there is no source code anywhere to be
found which makes the app **not verifiable**.
