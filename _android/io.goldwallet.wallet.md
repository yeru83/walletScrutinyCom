---
wsId: 
title: "GoldWallet - Bitcoin Vault Wallet"
altTitle: 
authors:

users: 10000
appId: io.goldwallet.wallet
launchDate: 
latestUpdate: 2021-04-28
apkVersionName: "6.2.0"
stars: 4.2
ratings: 444
reviews: 253
size: 36M
website: https://bitcoinvault.global
repository: 
issue: 
icon: io.goldwallet.wallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.goldwallet.wallet/
---


This app appears to not be a vault for Bitcoin but something for Bitcoin Vault.
