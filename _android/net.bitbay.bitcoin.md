---
wsId: bitpaytrading
title: "Bitcoin & Crypto Exchange - BitBay"
altTitle: 
authors:
- leo
users: 100000
appId: net.bitbay.bitcoin
launchDate: 
latestUpdate: 2021-04-02
apkVersionName: "1.1.18"
stars: 3.1
ratings: 841
reviews: 446
size: 16M
website: https://bitbay.net
repository: 
issue: 
icon: net.bitbay.bitcoin.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BitBay
providerLinkedIn: bitbay
providerFacebook: BitBay
providerReddit: 

redirect_from:
  - /net.bitbay.bitcoin/
---


BitPay is an old player in the space and is best known as a payment processor.
This app's description loses no word on who holds the keys to your coins but on
their website we can read:

> **Funds safety**<br>
  We keep all cryptocurrency funds on so called cold wallets. It means they are
  not connected to exchange servers directly.

which means this app is a custodial offering and therefore **not verifiable**.
