---
wsId: 
title: "Blockchain.com Wallet Pro-buy bitcoin Eth & crypto"
altTitle: 
authors:
- leo
users: 5000
appId: jb.tech.blockchainpro
launchDate: 
latestUpdate: 2021-04-25
apkVersionName: "2.5"
stars: 4.6
ratings: 203
reviews: 194
size: 9.4M
website: 
repository: 
issue: 
icon: jb.tech.blockchainpro.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-30
reviewStale: true
signer: 
reviewArchive:
- date: 2021-04-24
  version: 
  apkHash: 
  gitRevision: 7c41675d933938883582fc5a083d69e8b2644900
  verdict: wip


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-04-30**: This app is no more and probably scammed some people as
the original analysis below suggests.

### Original Analysis

This app is a fake version of
[Blockchain.com Wallet - Buy Bitcoin, ETH, & Crypto](/android/piuk.blockchain.android).

As most of those scams, it uses some fake reviews but they are using a variation
of the old logo of Blockchain.com's wallet and the provider is

`Blockchain Luxembourg L.A.` instead of<br>
`Blockchain Luxembourg S.A.`

For lack of a scam verdict, we leave this as WIP for now and assume it'll be
defunct in no time.
