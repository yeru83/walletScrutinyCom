---
wsId: 
title: "Zebpay Bitcoin and Cryptocurrency Exchange"
altTitle: 
authors:
- leo
users: 1000000
appId: zebpay.Application
launchDate: 2014-12-23
latestUpdate: 2021-04-09
apkVersionName: "3.13.00"
stars: 3.4
ratings: 77580
reviews: 37396
size: 11M
website: https://www.zebpay.com
repository: 
issue: 
icon: zebpay.Application.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: zebpay
providerLinkedIn: zebpay
providerFacebook: zebpay
providerReddit: 

redirect_from:
  - /zebpay.Application/
  - /posts/zebpay.Application/
---


In the description at Google Play we read:

> We use industry leading practice of maintaining the majority of customer
  cryptos offline

This app is an interface for an exchange and as such, only a window into what
you have in your account at that exchange. As a custodial wallet or bitcoin
bank it is **not verifiable**.
