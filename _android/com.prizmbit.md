---
wsId: prizmbit
title: "Prizm wallet, p2p cryptocurrency exchange"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.prizmbit
launchDate: 
latestUpdate: 2021-04-09
apkVersionName: "1.4.18"
stars: 3.8
ratings: 477
reviews: 225
size: 13M
website: https://prizmbit.com/
repository: 
issue: 
icon: com.prizmbit.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: prizmbit
providerLinkedIn: 
providerFacebook: prizmbit
providerReddit: 

redirect_from:

---


There is no statement regarding how private keys are managed in the play store description or on the [providers website](https://prizmbit.com/) or FAQ.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.
