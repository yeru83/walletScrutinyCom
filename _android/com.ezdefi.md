---
wsId: ezdefi
title: "ezDeFi - Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.ezdefi
launchDate: 
latestUpdate: 2021-04-19
apkVersionName: "0.3.3"
stars: 4.3
ratings: 572
reviews: 343
size: 50M
website: https://ezdefi.com/
repository: 
issue: 
icon: com.ezdefi.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ezDeFi
providerLinkedIn: 
providerFacebook: ezdefi
providerReddit: 

redirect_from:

---


Features like

> By eliminating encryption phrase, new users can simply make purchases with
  just a wallet password or biometric.

sound very custodial. Although this is

> A new Ez Mode [...] to make cryptocurrencies accessible to new users.

there are no explicit claims about the app being non-custodial otherwise, which
is why we have to assume it's custodial all the way and thus **not verifiable**.
