---
wsId: 
title: "Krypto - Bitcoin, Crypto Trading Exchange India"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.krypto
launchDate: 
latestUpdate: 2021-05-02
apkVersionName: "8.5"
stars: 4.3
ratings: 887
reviews: 542
size: 9.1M
website: https://letskrypto.com
repository: 
issue: 
icon: com.krypto.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: letskrypto
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


The Krypto wallet has no statements on [their website](https://letskrypto.com) regarding the management of private keys.
this leads us to conclude the wallet funds are likely under the control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
