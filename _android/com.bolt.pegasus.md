---
wsId: 
title: "BOLT X"
altTitle: 
authors:
- kiwilamb
users: 5000
appId: com.bolt.pegasus
launchDate: 
latestUpdate: 2021-03-11
apkVersionName: "1.19.0"
stars: 4.7
ratings: 354
reviews: 292
size: 17M
website: https://bolt.global/
repository: 
issue: 
icon: com.bolt.pegasus.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bolt_global
providerLinkedIn: bolt-global
providerFacebook: Global.Bolt
providerReddit: 

redirect_from:

---


Seems like a secondary project called "Pegasus" of Bolt Global where you can earn BOLT tokens into the Bolt-X wallet from using the Bolt+ interactive media companion app.

*(Besides that, we couldn't find any source code or even a claim of it being non-custodial.)*
