---
wsId: valr
title: "VALR - Bitcoin Exchange & Cryptocurrency Wallet"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.valr.app
launchDate: 
latestUpdate: 2021-04-19
apkVersionName: "1.0.25"
stars: 4.3
ratings: 529
reviews: 290
size: 89M
website: https://www.valr.com
repository: 
issue: 
icon: com.valr.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-13
reviewStale: true
signer: 
reviewArchive:


providerTwitter: valrdotcom
providerLinkedIn: valr
providerFacebook: VALRdotcom
providerReddit: 

redirect_from:

---


I need not go further into researching this wallet as the statement on the Google Play description screams custodial.

> We hold your cryptocurrencies in both “cold storage” and “hot wallets”.

This is an exchange trading wallet that holds the customers funds in the providers control.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

