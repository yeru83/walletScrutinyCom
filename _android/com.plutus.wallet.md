---
wsId: goabra
title: "Abra Bitcoin Wallet Buy Trade Earn Interest Borrow"
altTitle: 
authors:
- leo
users: 1000000
appId: com.plutus.wallet
launchDate: 2015-03-04
latestUpdate: 2021-04-28
apkVersionName: "Varies with device"
stars: 4.6
ratings: 19741
reviews: 6804
size: Varies with device
website: https://www.abra.com
repository: 
issue: 
icon: com.plutus.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AbraGlobal
providerLinkedIn: abra
providerFacebook: GoAbraGlobal
providerReddit: 

redirect_from:
  - /com.plutus.wallet/
  - /posts/com.plutus.wallet/
---


The Google Play description certainly sounds like a custodial wallet:

> Abra is the world’s first global investment app that enables you to invest in
hundreds of cryptocurrencies* like Bitcoin, Ethereum, XRP, Litecoin, Stellar,
Monero, and many more all in one app.

As we can't find a word on security or their source code or about the user being
in control we conclude for now this is a custodial app which gives it our
verdict: **not verifiable**.
