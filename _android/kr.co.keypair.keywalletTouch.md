---
wsId: krcokeypair
title: "KeyWallet Touch - Bitcoin Ethereum Crypto Wallet"
altTitle: 
authors:

users: 10000
appId: kr.co.keypair.keywalletTouch
launchDate: 
latestUpdate: 2020-12-14
apkVersionName: "Varies with device"
stars: 4.4
ratings: 87
reviews: 58
size: Varies with device
website: https://keywalletpro.io
repository: 
issue: 
icon: kr.co.keypair.keywalletTouch.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


The description makes rather vague claims:

> - Fully complied with HD(Hierarchical Deterministic) wallet

which is a standard that makes most sense for self-custodial wallets.

> This application manages crypto wallets for Bitcoin, Ethereum, Ripple, ERC20
  tokens and etc.

Now this might be a language barrier issue but we would hope it is a wallet and
not an application to manage wallets aka "not a wallet"?

Their website appears to be no more and we get forwarded to
[some Korean site](http://html.ugo.kr/servicestop.html) that according to Google
translate reads:

> This is to inform you that the "service period has expired" for this site .

There is another website though: [afinkeywallet.io](https://afinkeywallet.io)

So in the best of cases this is a functioning closed source self-custodial
Bitcoin wallet and thus **not verifiable**.
