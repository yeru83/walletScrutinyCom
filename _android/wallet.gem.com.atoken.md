---
wsId: ATokenWallet
title: "ATokenWallet"
altTitle: 
authors:
- kiwilamb
users: 1000000
appId: wallet.gem.com.atoken
launchDate: 
latestUpdate: 2021-04-28
apkVersionName: "4.1.0"
stars: 4.0
ratings: 3846
reviews: 2412
size: 55M
website: https://www.atoken.com
repository: 
issue: 
icon: wallet.gem.com.atoken.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ATokenOfficial
providerLinkedIn: 
providerFacebook: ATokenOfficial
providerReddit: 

redirect_from:

---


Found on their support website...

> The AToken Wallet server does not save any private keys, mnemonics, and
  passwords for users, so mnemonics or lost private keys cannot be retrieved
  from the AToken wallet.<br>
  Please make sure that all users make backups and keep them properly. Do not
  share them with anyone.

The claim on their website is that the wallet is non-custodial, but without source code, this is **not verifiable**.
