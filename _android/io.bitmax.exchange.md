---
wsId: ascendex
title: "AscendEX(BitMax)"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: io.bitmax.exchange
launchDate: 
latestUpdate: 2021-04-25
apkVersionName: "2.4.7"
stars: 4.3
ratings: 2754
reviews: 929
size: 22M
website: https://ascendex.com
repository: 
issue: 
icon: io.bitmax.exchange.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: AscendEX_Global
providerLinkedIn: 
providerFacebook: AscendEXOfficial
providerReddit: AscendEX_Official

redirect_from:

---


The AscendEx mobile app claims on the website help section to manage bitcoins...

> You can withdraw your digital assets to external platforms or wallets via their address. Copy the address from the external platform or wallet, and paste it into the withdrawal address field on AscendEX to complete the withdrawal. 

however their is no evidence of the wallet being non-custodial, this leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

