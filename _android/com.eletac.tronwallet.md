---
wsId: visionwallet
title: "Vision: Bitcoin and Crypto Wallet"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.eletac.tronwallet
launchDate: 
latestUpdate: 2021-05-02
apkVersionName: "1.2.7"
stars: 4.6
ratings: 1141
reviews: 595
size: 96M
website: https://www.vision-crypto.com
repository: 
issue: 
icon: com.eletac.tronwallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: VisionCryptoApp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


We do not have to look far to find the wallet claims to be non-custodial.

> When you create a wallet, it is very important that you store your received recovery phrase safely, because nobody but you has and should have access to your wallet.

However such claims need to be verified and this wallets source code is nowhere to be found.

Our verdict: This ‘wallet’ claims to be non-custodial, however with no source code this wallet is **not verifiable**.

