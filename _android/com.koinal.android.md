---
wsId: koinal
title: "Koinal: Buy Bitcoin instantly"
altTitle: 
authors:
- leo
users: 10000
appId: com.koinal.android
launchDate: 
latestUpdate: 2021-04-24
apkVersionName: "1.2.5"
stars: 4.1
ratings: 334
reviews: 238
size: 30M
website: https://www.koinal.io
repository: 
issue: 
icon: com.koinal.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: koinal_io
providerLinkedIn: koinal-io
providerFacebook: Koinal.io
providerReddit: 

redirect_from:
  - /com.koinal.android/
---


> We take our system’s security and user safety extremely seriously. All Koinal systems use 256BIT RAPID SSL and Google two factor authentication. We can proudly state that our systems are extremely secure and we have a laser focus on protecting your data and investments!

This is their statement on security but it reads more like they are talking
about their servers than their wallet here. That would imply a custodial wallet.

A user [wrote](https://play.google.com/store/apps/details?id=com.koinal.android&reviewId=gp%3AAOqpTOF00ZzwGqBPZshWKuaWeQMjIIth50RPb72hiGVl58xWNUb4S0P0NwwZIl0avKF00U_wua5iL26G0B2CYQ):

> Stay away, collects all your information THEN tells you it does not accept
  your country. SCAM!

A Bitcoin wallet should not care about borders, especially not if it's
non-custodial but maybe the user meant some buying option?

As there are no further claims on the website neither, we assume the app is
custodial and thus **not verifiable**.
