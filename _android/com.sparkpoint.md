---
wsId: 
title: "SparkPoint - Cryptocurrency Wallet & DApps Browser"
altTitle: 
authors:
- leo
- emanuel
users: 10000
appId: com.sparkpoint
launchDate: 
latestUpdate: 2021-03-30
apkVersionName: "6.3.1"
stars: 3.0
ratings: 481
reviews: 414
size: 13M
website: https://sparkpoint.io/
repository: 
issue: 
icon: com.sparkpoint.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: sparkpointio
providerLinkedIn: sparkpointio
providerFacebook: sparkpointio
providerReddit: SparkPoint

redirect_from:

---


This app sounds at first as if it was not for Bitcoin but self-custodial:

> In this initial release of SparkPoint, we are first enabling SparkPoint
  Wallet, a 100% non-custodial wallet, so you can store your ETH and SRK tokens
  on your Android devices.

but further down we can also find Bitcoin:

> - Send and receive Bitcoin (BTC)

**The reviews are brutal!!** Read the scam accusations and consider that
positive ratings might be bought! This amount of accusations is not normal!

That said, we can't find any source code and conclude the app is **not verifiable**.
