---
wsId: 
title: "SOLO Wallet"
altTitle: 
authors:

users: 5000
appId: com.sologenicwallet
launchDate: 
latestUpdate: 2021-04-03
apkVersionName: "2.0.3"
stars: 3.6
ratings: 69
reviews: 46
size: 53M
website: 
repository: 
issue: 
icon: com.sologenicwallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.sologenicwallet/
  - /posts/com.sologenicwallet/
---


This wallet does not support BTC.
