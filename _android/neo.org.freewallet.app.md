---
wsId: 
title: "NEO Wallet. Send & Receive the coin－Freewallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: neo.org.freewallet.app
launchDate: 
latestUpdate: 2020-04-21
apkVersionName: "2.5.2"
stars: 4.2
ratings: 201
reviews: 144
size: 7.4M
website: https://freewallet.org/
repository: 
issue: 
icon: neo.org.freewallet.app.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This wallet is built for the management of an alt coin, NEO token.

Our verdict: This is not a wallet for holding bitcoins.

