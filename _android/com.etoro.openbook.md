---
wsId: etoro
title: "eToro - Smart crypto trading made easy"
altTitle: 
authors:
- leo
users: 5000000
appId: com.etoro.openbook
launchDate: 2013-11-05
latestUpdate: 2021-04-22
apkVersionName: "317.0.0"
stars: 4.2
ratings: 79259
reviews: 31076
size: 56M
website: https://www.etoro.com
repository: 
issue: 
icon: com.etoro.openbook.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: etoro
providerLinkedIn: etoro
providerFacebook: eToro
providerReddit: 

redirect_from:

---


Etoro is used to speculate on assets more than to actually transfer them but in
the case of Bitcoin, according to
[the Help Center](https://www.etoro.com/customer-service/help/1422157482/can-i-withdraw-my-cryptocurrencies-from-the-platform/)
you can actually send Bitcoins from this app ... if you are in the right
jurisdiction ...
[further restrictions apply](https://etoro.nanorep.co/widget/widget.html?kb=156763&account=etoro#onloadquestionid=1306615492) ...

So all in all this could pass as a custodial app.

As a custodial app it is **not verifiable**.
