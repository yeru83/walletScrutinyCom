---
wsId: 
title: "Pillar"
altTitle: 
authors:

users: 10000
appId: com.pillarproject.wallet
launchDate: 2018-12-13
latestUpdate: 2021-04-12
apkVersionName: "2.34.12"
stars: 3.4
ratings: 420
reviews: 224
size: 51M
website: https://pillarproject.io
repository: https://github.com/pillarwallet/pillarwallet
issue: 
icon: com.pillarproject.wallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-02-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: PillarWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.pillarproject.wallet/
  - /posts/com.pillarproject.wallet/
---


This app is not a Bitcoin wallet. Neither the description nor the website claim
support of BTC and when installing it, you can find tokens with "Bitcoin" in
their name that can be managed with this app but none of them actually is Bitcoin.
