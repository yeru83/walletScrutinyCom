---
wsId: coinbasepro
title: "Coinbase Pro – Bitcoin & Crypto Trading"
altTitle: 
authors:
- leo
users: 500000
appId: com.coinbase.pro
launchDate: 
latestUpdate: 2021-04-16
apkVersionName: "1.0.72"
stars: 4.2
ratings: 9310
reviews: 3345
size: 57M
website: https://pro.coinbase.com
repository: 
issue: 
icon: com.coinbase.pro.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinbasePro
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:
  - /com.coinbase.pro/
  - /posts/com.coinbase.pro/
---


This is the interface for a trading platform aka exchange. The funds are stored
with the provider. As a custodial service it is **not verifiable**.
