---
wsId: phemex
title: "Phemex: Top Bitcoin Exchange App, Crypto & 0 Fees"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.phemex.app
launchDate: 
latestUpdate: 2021-04-10
apkVersionName: "1.3.2"
stars: 4.0
ratings: 6185
reviews: 1905
size: 17M
website: https://phemex.com
repository: 
issue: 
icon: com.phemex.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-13
reviewStale: true
signer: 
reviewArchive:


providerTwitter: phemex_official
providerLinkedIn: phemex
providerFacebook: Phemex.official
providerReddit: 

redirect_from:

---


The Phemex mobile app claims to hold funds in cold storage...

> All assets are 100% stored in cold wallets. Each withdrawal is thoroughly monitored and requires two-person approval with offline signatures.

leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
