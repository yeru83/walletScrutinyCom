---
wsId: 
title: "Ducatus Wallet"
altTitle: 
authors:
- leo
users: 5000
appId: io.ducatus.walnew
launchDate: 
latestUpdate: 2021-04-13
apkVersionName: "2.6.3"
stars: 4.9
ratings: 193
reviews: 130
size: 25M
website: https://ducatus.net
repository: 
issue: 
icon: io.ducatus.walnew.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app supports Bitcoin:

> Now supporting Bitcoin, Bitcoin Cash and Ethereum.

The ten reviews are all one or two words 5 stars except for
[the one 1 star review](https://play.google.com/store/apps/details?id=io.ducatus.walnew&reviewId=gp%3AAOqpTOGugVoUMzb6SVIMKYq_-dA9r_fCDED3Xj5qehzQALl_tVyXXhtylPIQYG2VnTwXqpxN28bLslG8Mk4G-A):

> Re Invest<br>
  ★☆☆☆☆ 12 May 2020<br>
  Hey guy.. all coin in wallet disappeared? What 's ?

Their website is also "suspended". There is no further claims anywhere
to be found so we have to assume this app is not public source, not
self-custodial and **not verifiable** and also **probably not safe to use**.
