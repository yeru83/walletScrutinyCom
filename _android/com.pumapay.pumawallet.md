---
wsId: PumaPay
title: "PumaPay Blockchain Wallet 4 Bitcoin & Crypto Coins"
altTitle: 
authors:
- leo
users: 10000
appId: com.pumapay.pumawallet
launchDate: 
latestUpdate: 2021-03-23
apkVersionName: "3.9.5"
stars: 3.9
ratings: 326
reviews: 214
size: 60M
website: https://pumapay.io
repository: 
issue: 
icon: com.pumapay.pumawallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: PumaPay
providerLinkedIn: decentralized-vision
providerFacebook: PumaPayOfficial
providerReddit: 

redirect_from:
  - /com.pumapay.pumawallet/
  - /posts/com.pumapay.pumawallet/
---


This app has very little information on their website but in the Google Play
description we read:

> Please note: It is your sole responsibility to keep the 12-word seed phrase of
  the wallet in a safe place for you to be able to access it in the future. In
  this case, you will be required to restore the Private Key.

So this sounds like they claim to be non-custodial but we cannot see any source
code which makes the app **not verifiable**.
