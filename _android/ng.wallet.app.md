---
wsId: WalletsAfrica
title: "Wallets Africa - Seamless Digital Transactions"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: ng.wallet.app
launchDate: 
latestUpdate: 2021-05-01
apkVersionName: "2.472"
stars: 4.2
ratings: 1404
reviews: 1154
size: 16M
website: 
repository: 
issue: 
icon: ng.wallet.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: walletsafrica
providerLinkedIn: 
providerFacebook: walletsafrica
providerReddit: 

redirect_from:

---


Wallets Africa is quite a broad product, the lack of source code makes it
impossible to verify this app and there are no statements on their website as to
management of private keys.

Our verdict: This “wallet” is probably custodial and therefore is
**not verifiable**.
