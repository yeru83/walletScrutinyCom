---
wsId: 
title: "eXchangily DEX Bitcoin Wallet & Crypto Wallet"
altTitle: 
authors:
- emanuel
- leo
users: 1000
appId: com.exchangily.wallet
launchDate: 
latestUpdate: 2021-04-27
apkVersionName: "2.0.18"
stars: 4.1
ratings: 26
reviews: 18
size: 16M
website: https://exchangily.com
repository: https://github.com/blockchaingate/exchangily-mobile-app
issue: https://github.com/blockchaingate/exchangily-mobile-app/issues/1
icon: com.exchangily.wallet.png
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ExchangilyC
providerLinkedIn: 
providerFacebook: Exchangily-439040053240813
providerReddit: 

redirect_from:

---


This app is a self-custodial Bitcoin wallet:

> With eXchangily, you own and control your private key, ensuring your crypto
  assets are kept in your wallet, giving you absolute control and 100%
  autonomous ownership.

They are also open source:

> FAB and eXchangily are open source projects, unlike other exchanges we have
  nothing to hide, anyone can see our code at any time.

Notably their website does not link to this Play Store app!

But unfortunately there are no build instructions and
[Emanuel failed to build it](https://github.com/blockchaingate/exchangily-mobile-app/issues/1)
and I have no further ideas to make it build after all. This app is **not verifiable**.
