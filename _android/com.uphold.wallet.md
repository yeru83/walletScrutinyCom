---
wsId: UpholdbuyandsellBitcoin
title: "Uphold: buy and sell Bitcoin"
altTitle: 
authors:
- leo
users: 1000000
appId: com.uphold.wallet
launchDate: 
latestUpdate: 2021-04-30
apkVersionName: "4.15.23"
stars: 3.2
ratings: 13398
reviews: 8158
size: 45M
website: https://uphold.com
repository: 
issue: 
icon: com.uphold.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: UpholdInc
providerLinkedIn: upholdinc
providerFacebook: UpholdInc
providerReddit: 

redirect_from:
  - /com.uphold.wallet/
  - /posts/com.uphold.wallet/
---


This app appears to be an interface to a custodial trading platform. In the
Google Play description we read:

> Trust Through Transparency
> Uphold is fully reserved. Unlike banks, we don’t loan out your money. To prove
  it, we publish our holdings in real time.

If they hold your money, you don't. As a custodial service this app is **not
verifiable**.
