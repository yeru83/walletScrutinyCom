---
wsId: 
title: "Coin98 Wallet - Crypto Wallet & Payment Gateway"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: coin98.crypto.finance.media
launchDate: 
latestUpdate: 2021-05-02
apkVersionName: "9.3.1"
stars: 4.7
ratings: 5143
reviews: 3746
size: 63M
website: https://coin98.app/
repository: 
issue: 
icon: coin98.crypto.finance.media.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coin98_wallet
providerLinkedIn: 
providerFacebook: Coin98Wallet
providerReddit: 

redirect_from:

---


The [Coin98 website](https://coin98.app/) claims to be a wallet from their description...

> Coin98 Wallet is the simplest and most secure crypto & bitcoin wallet to store, send, receive and manage your crypto assets, such as Bitcoin (BTC), Ethereum (ETH), TomoChain (TOMO), Ethereum-based ERC20 tokens. etc.

so it claims to manage BTC, it also claims users are in control of their private keys, hence the wallet claim of being non-custodial

> Taking full control of your assets with Biometric Authentication and Private Keys: managed by you, none of your personal data collected by the App.

with no source code repository listed or found...

Our verdict: This 'wallet' claims to be non-custodial but does not provide public source and therefore is **not verifiable**.
