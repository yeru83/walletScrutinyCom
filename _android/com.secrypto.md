---
wsId: 
title: "Secrypto-Bitcoin, ETH, EOS"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.secrypto
launchDate: 
latestUpdate: 2019-02-26
apkVersionName: "1.28"
stars: 2.9
ratings: 958
reviews: 653
size: 50M
website: https://www.secrypto.io
repository: 
issue: 
icon: com.secrypto.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-21
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


We do not have to look far to find the wallet claims to be non-custodial.

> *Highest-Level of Security. Sensitive keys stored encrypted and only locally on your devices. 

However such claims need to be verified and this wallets source code is nowhere to be found.
In fact we were not able to access [the wallets website](https://www.secrypto.io/) given a 404 response at time of review.

Our verdict: This 'wallet' claims to be non-custodial, however with no source code this wallet is **not verifiable**.
