---
wsId: metamask
title: "MetaMask - Buy, Send and Swap Crypto"
altTitle: 
authors:
- leo
users: 1000000
appId: io.metamask
launchDate: 
latestUpdate: 2021-04-21
apkVersionName: "2.1.3"
stars: 3.2
ratings: 4008
reviews: 2224
size: 28M
website: https://metamask.io
repository: 
issue: 
icon: io.metamask.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is an ETH-only app and thus not a Bitcoin wallet.
