---
wsId: yellowcard
title: "Yellow Card: Buy and Sell Bitcoin"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: io.yellowcard.app
launchDate: 
latestUpdate: 2021-04-23
apkVersionName: "2.3"
stars: 2.8
ratings: 721
reviews: 552
size: 1.2M
website: https://yellowcard.io/
repository: 
issue: 
icon: io.yellowcard.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-13
reviewStale: true
signer: 
reviewArchive:


providerTwitter: yellowcard_app
providerLinkedIn: yellowcardapp
providerFacebook: yellowcardapp
providerReddit: 

redirect_from:

---


The Yellow Card wallet has no statements on [their website](https://yellowcard.io/) or in their FAQ regarding the management of private keys.
This leads us to conclude the wallet funds are likely under the control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

