---
wsId: 
title: "WazirX - Bitcoin, Crypto Trading Exchange India"
altTitle: 
authors:
- kiwi1amb
users: 1000000
appId: com.wrx.wazirx
launchDate: 
latestUpdate: 2021-04-30
apkVersionName: "2.13.1"
stars: 4.2
ratings: 89680
reviews: 27440
size: 6.1M
website: 
repository: 
issue: 
icon: com.wrx.wazirx.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This mobile app is an exchange based trading solution and is not a wallet.
