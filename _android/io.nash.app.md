---
wsId: nash
title: "Nash – Buy crypto at the best rates"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: io.nash.app
launchDate: 
latestUpdate: 2021-04-26
apkVersionName: "6.0"
stars: 4.4
ratings: 604
reviews: 250
size: 67M
website: https://nash.io/
repository: 
issue: 
icon: io.nash.app.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: nashsocial
providerLinkedIn: nashsocial
providerFacebook: 
providerReddit: 

redirect_from:

---


This statement in the description from the [play store](https://play.google.com/store/apps/details?id=io.nash.app) below is a claim only the user has access to the private keys.

> Nash doesn’t take control of your funds – unlike Coinbase, Kraken or Binance. We’re the only fully-featured exchange where you can trade Bitcoin without giving up your private keys.

With keys in control of the user, we need to find the source code in order to check reproducibility.
However we are unable to locate a public source repository.

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.
