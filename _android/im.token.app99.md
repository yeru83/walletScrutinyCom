---
wsId: 
title: "AllToken : Bitcoin,Ethereum,Blockchain Wallet"
altTitle: 
authors:

users: 1000
appId: im.token.app99
launchDate: 
latestUpdate: 2020-05-09
apkVersionName: "2.7.3"
stars: 0.0
ratings: 
reviews: 
size: 46M
website: 
repository: 
issue: 
icon: im.token.app99.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-13
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app probably was related to
[imToken - Bitcoin & Ethereum Wallet](/android/im.token.app) as it has almost
the same `applicationId` but we did not get to look closer into it.
