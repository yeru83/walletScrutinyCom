---
wsId: Quidax
title: "Quidax - Buy and Sell Bitcoin"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.quidax.app
launchDate: 
latestUpdate: 2021-01-12
apkVersionName: "1.8.0"
stars: 2.8
ratings: 1507
reviews: 1112
size: 53M
website: https://www.quidax.com
repository: 
issue: 
icon: com.quidax.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: QuidaxAfrica
providerLinkedIn: quidax
providerFacebook: quidaxafrica
providerReddit: 

redirect_from:

---


From the play store description it clearly states the private keys are in control of the provider....

> Also, 98% of all cryptocurrencies on Quidax are stored in cold wallets (offline) with the remaining 2% protected in line with security best practises.

This means the exchange is holding the bitcoins on behalf of the user, hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
