---
wsId: atomex
title: "Atomex - Crypto Wallet & Atomic swap DEX"
altTitle: 
authors:
- leo
users: 1000
appId: com.atomex.android
launchDate: 
latestUpdate: 2021-03-25
apkVersionName: "1.7"
stars: 4.8
ratings: 34
reviews: 33
size: 59M
website: https://atomex.me
repository: https://github.com/atomex-me/atomex.mobile
issue: https://github.com/atomex-me/atomex.mobile/issues/24
icon: com.atomex.android.png
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: atomex_official
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> Atomex — is a non-custodial crypto wallet

and it also supports Bitcoin and ...

> Atomex is an open-source project by the Baking Bad team, which is known in the
  Tezos community as one of the most active ecosystem contributors.

and indeed their linked GitHub account has a repository that looks promising:
[atomex-me/atomex.mobile](https://github.com/atomex-me/atomex.mobile).

Unfortunately at this point I have to give up as this is the first project we
review that was built in Visual Studio using C#. As there are no build
instructions I can only hope for
[help from the provider](https://github.com/atomex-me/atomex.mobile/issues/24)
and conclude for now that the app is **not verifiable**.
