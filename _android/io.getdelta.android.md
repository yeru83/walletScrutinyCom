---
wsId: 
title: "Delta Investment Portfolio Tracker"
altTitle: 
authors:

users: 500000
appId: io.getdelta.android
launchDate: 
latestUpdate: 2021-04-01
apkVersionName: "4.1.0"
stars: 4.6
ratings: 21396
reviews: 7593
size: 40M
website: 
repository: 
issue: 
icon: io.getdelta.android.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.getdelta.android/
---


This appears to be only a portfolio tracker. If it asks for your credentials for
exchanges, it might still get into a position of pulling your funds from there.
