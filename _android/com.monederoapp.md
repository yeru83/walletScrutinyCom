---
wsId: 
title: "Monedero: Wallet Bitcoin, Ethereum, Dash"
altTitle: 
authors:
- leo
users: 1000
appId: com.monederoapp
launchDate: 
latestUpdate: 2021-04-20
apkVersionName: "3.1.2"
stars: 4.2
ratings: 13
reviews: 5
size: 12M
website: 
repository: 
issue: 
icon: com.monederoapp.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.monederoapp/
  - /posts/com.monederoapp/
---


This page was created by a script from the **appId** "com.monederoapp" and public
information found
[here](https://play.google.com/store/apps/details?id=com.monederoapp).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.
