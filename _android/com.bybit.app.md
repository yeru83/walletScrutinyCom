---
wsId: bybit
title: "Bybit: Crypto Trading & Bitcoin Futures App"
altTitle: 
authors:
- leo
users: 500000
appId: com.bybit.app
launchDate: 
latestUpdate: 2021-04-20
apkVersionName: "2.0.5"
stars: 4.3
ratings: 3245
reviews: 1493
size: 44M
website: https://www.bybit.com
repository: 
issue: 
icon: com.bybit.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-09
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Bybit_Official
providerLinkedIn: bybitexchange
providerFacebook: Bybit
providerReddit: Bybit

redirect_from:

---


> "Bybit is the World's fastest-growing and one of the largest crypto
  derivatives exchanges to trade Bitcoin and crypto.

and as such, funds are in cold storage with them:

> YOUR SAFETY IS OUR PRIORITY<br>
  We safeguard your cryptocurrencies with a multi-signature cold-wallet
  solution. Your funds are 100% protected from the prying eyes. All traders'
  deposited assets are segregated from Bybit's operating budget to increase our
  financial accountability and transparency.

As a custodial app it is **not verifiable**.
