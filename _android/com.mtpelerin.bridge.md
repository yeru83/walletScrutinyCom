---
wsId: 
title: "Bridge Wallet, the Swiss app for Bitcoin & Crypto"
altTitle: 
authors:
- leo
users: 1000
appId: com.mtpelerin.bridge
launchDate: 
latestUpdate: 2021-04-19
apkVersionName: "1.13"
stars: 4.6
ratings: 47
reviews: 28
size: 76M
website: https://www.mtpelerin.com/bridge-wallet
repository: 
issue: 
icon: com.mtpelerin.bridge.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-27
reviewStale: true
signer: 
reviewArchive:


providerTwitter: mtpelerin
providerLinkedIn: mt-pelerin
providerFacebook: mtpelerin
providerReddit: MtPelerin

redirect_from:
  - /com.mtpelerin.bridge/
---


On Google Play they claim

> **YOU ARE IN CONTROL**<br>
  Bridge Wallet is a decentralized, non-custodial wallet. It means that you are
  in full control of your wallet, its content and seed phrase.

But while the provider [has a GitHub presence](https://github.com/MtPelerin),
there is no claim about public source and neither do we find any wallet
repository on their GitHub.

As a closed source wallet, this is **not verifiable**.
