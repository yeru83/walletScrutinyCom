---
wsId: midas
title: "Midas Crypto Wallet: Bitcoin, Ethereum, XRP, EOS"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.midasprotocol.wallet.android
launchDate: 
latestUpdate: 2021-04-21
apkVersionName: "2.0"
stars: 4.6
ratings: 420
reviews: 257
size: 73M
website: https://midasprotocol.io/
repository: 
issue: 
icon: com.midasprotocol.wallet.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: MidasProtocol
providerLinkedIn: 
providerFacebook: midasprotocol.io
providerReddit: 

redirect_from:

---


No statements regarding private key managment can be found on the [providers website](https://midasprotocol.io/) or [Support section](https://support.midasprotocol.io/hc/en-us).
It would be prudent to assume the private keys are under the control of the provider.


Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
