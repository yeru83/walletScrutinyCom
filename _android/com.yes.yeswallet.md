---
wsId: 
title: "Yes Wallet"
altTitle: 
authors:
- leo
users: 5000
appId: com.yes.yeswallet
launchDate: 
latestUpdate: 2019-04-24
apkVersionName: "1.0"
stars: 2.8
ratings: 41
reviews: 28
size: 1.5M
website: http://www.yeswallet.io
repository: 
issue: 
icon: com.yes.yeswallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.yes.yeswallet/
---


This app is **very likely a scam** judging by the reviews and the website being
down and the last update being almost two years old.

For what we know, we know little but the coins are probably under the control of
the provider, so the app is also **not verifiable**.
