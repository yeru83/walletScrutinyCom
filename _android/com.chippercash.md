---
wsId: 
title: "Chipper Cash - Send & Receive Money Across Africa"
altTitle: 
authors:
- kiwilamb
users: 1000000
appId: com.chippercash
launchDate: 
latestUpdate: 2021-05-01
apkVersionName: "1.9.3"
stars: 4.1
ratings: 28419
reviews: 17907
size: Varies with device
website: https://chippercash.com/
repository: 
issue: 
icon: com.chippercash.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Stated in their sites [support article](https://support.chippercash.com/en/articles/4750740-how-to-buy-sell-cryptocurrency-on-chipper-cash) 
"Currently it's not possible to send to or receive Bitcoin or Ethereum from external wallets"

Conclusion is that Chipper is a custodial wallet as funds are held by Chipper on behalf of the user.
