---
wsId: mw.org.freewallet
title: "Freewallet: Bitcoin & Crypto Blockchain Wallet"
altTitle: 
authors:
- leo
users: 500000
appId: mw.org.freewallet.app
launchDate: 2017-08-10
latestUpdate: 2021-03-04
apkVersionName: "1.15.3"
stars: 3.8
ratings: 9332
reviews: 6263
size: 12M
website: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: 

redirect_from:

---


According to the description

> In addition, the majority of cryptocurrency assets on the platform are stored
  in an offline vault. Your coins will be kept in cold storage with state of the
  art security protecting them.

This is a custodial app.

Our verdict: **not verifiable**.
