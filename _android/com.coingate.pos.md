---
wsId: 
title: "Bitcoin POS"
altTitle: 
authors:
- kiwilamb
users: 5000
appId: com.coingate.pos
launchDate: 
latestUpdate: 2016-08-18
apkVersionName: "2.0.0"
stars: 3.8
ratings: 21
reviews: 8
size: 8.1M
website: https://coingate.com/
repository: 
issue: 
icon: com.coingate.pos.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is a POS solution aimed at Merchants receiving crypo currencies, the POS mobile app is listed on [their website](https://coingate.com/pos), however no mention of any source repository for it.
This mobile solution looks to work via an Api and can not send crypto currencies, hence it is not a wallet.
