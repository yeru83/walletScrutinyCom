---
wsId: coinhako
title: "Coinhako: Buy Bitcoin, Crypto Wallet & Trading"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.coinhako
launchDate: 
latestUpdate: 2021-04-05
apkVersionName: "2.2.4"
stars: 2.5
ratings: 836
reviews: 673
size: 48M
website: https://www.coinhako.com
repository: 
issue: 
icon: com.coinhako.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinhako
providerLinkedIn: coinhako
providerFacebook: coinhako
providerReddit: 

redirect_from:

---


Having a scan over the providers website and faq articles does not reveal any
claims regarding the management of private keys.
We would have to assume this wallet is custodial.

Our verdict: This “wallet” is probably custodial and therefore is **not verifiable**.
