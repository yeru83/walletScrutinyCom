---
wsId: kucoin
title: "KuCoin: Bitcoin Exchange & Crypto Wallet"
altTitle: 
authors:
- leo
users: 500000
appId: com.kubi.kucoin
launchDate: 
latestUpdate: 2021-04-26
apkVersionName: "3.32.0"
stars: 3.6
ratings: 5418
reviews: 3059
size: 48M
website: 
repository: 
issue: 
icon: com.kubi.kucoin.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-09
reviewStale: true
signer: 
reviewArchive:


providerTwitter: KuCoinCom
providerLinkedIn: kucoin
providerFacebook: KuCoinOfficial
providerReddit: kucoin

redirect_from:

---


> KuCoin is the most popular bitcoin exchange that you can buy and sell bitcoin
  securely.

This app is the interface to an exchange. Exchanges are all custodial which
makes the app **not verifiable**.
