---
wsId: 
title: "Multicurrency crypto-wallet: Bitpapa"
altTitle: 
authors:
- leo
users: 10000
appId: com.bitpapa
launchDate: 
latestUpdate: 2021-04-01
apkVersionName: "1.5.24"
stars: 4.9
ratings: 2788
reviews: 2753
size: 42M
website: https://bitpapa.com
repository: 
issue: 
icon: com.bitpapa.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-11
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bitpapa_com
providerLinkedIn: 
providerFacebook: bitpapacom
providerReddit: 

redirect_from:

---


This app supports Bitcoin:

> Bitpapa app allows you to create a multicurrency cryptowallet for secure
  transactions in Bitcoin, Ethereum, and USDT

You can send and receive:

> You can receive, send, and store cryptocurrencies using a convenient and
  secure cryptowallet within your account, as well as trade securely with other
  people on Bitpapa P2P marketplace

And ... it's custodial:

> Bitpapa has eliminated trading commissions, and internal transfers between
  Bitpapa users are free as well.

Free transactions works if it's updates to their database. The blockchain is not
free ever.

This app is **not verifiable**.
