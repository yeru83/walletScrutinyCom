---
wsId: 
title: "ELLIPAL: Crypto Bitcoin Wallet"
altTitle: 
authors:

users: 10000
appId: com.ellipal.wallet
launchDate: 
latestUpdate: 2021-04-23
apkVersionName: "2.9.5"
stars: 4.0
ratings: 391
reviews: 241
size: 17M
website: https://www.ellipal.com
repository: 
issue: 
icon: com.ellipal.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ellipalwallet
providerLinkedIn: 
providerFacebook: ellipalclub
providerReddit: ELLIPAL_Official

redirect_from:

---


This app come with the claim:

> Secure HD wallet for cryptocurrencies. Store, transact, and trade Bitcoin and
  Crypto: BTC ETH LTC DGB BSV BAT OMG XRP XVG & 1000+ more.

and absent more explicit claims, we have to guess that HD means "hierarchically
deterministic", a standard for self-custodial wallets.

As we can't find a source code repository on their website or
[their company GitHub account](https://github.com/ELLIPAL?tab=repositories&type=source),
we assume the app is closed source and thus **not verifiable**.
