---
wsId: coincorner
title: "CoinCorner – Buy & sell bitcoin. Crypto Wallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.coincorner.app.crypt
launchDate: 
latestUpdate: 2021-04-12
apkVersionName: "3.2.9"
stars: 2.9
ratings: 151
reviews: 106
size: 22M
website: 
repository: 
issue: 
icon: com.coincorner.app.crypt.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinCorner
providerLinkedIn: 
providerFacebook: CoinCorner
providerReddit: 

redirect_from:

---


A search of the play store and the providers website, reveals no statements about how private keys are managed.

This leads us to conclude the wallets funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

