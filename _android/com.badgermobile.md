---
wsId: 
title: "Badger Wallet"
altTitle: 
authors:

users: 10000
appId: com.badgermobile
launchDate: 2019-06-10
latestUpdate: 2020-03-04
apkVersionName: "1.12.1"
stars: 3.8
ratings: 145
reviews: 75
size: 10M
website: https://badger.bitcoin.com
repository: 
issue: 
icon: com.badgermobile.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: badgerwallet
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: 

redirect_from:
  - /com.badgermobile/
  - /posts/com.badgermobile/
---


