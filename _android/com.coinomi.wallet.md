---
wsId: coinomi
title: "Coinomi Wallet :: Bitcoin Ethereum Altcoins Tokens"
altTitle: 
authors:
- leo
users: 1000000
appId: com.coinomi.wallet
launchDate: 2014-01-31
latestUpdate: 2021-04-15
apkVersionName: "Varies with device"
stars: 4.7
ratings: 35111
reviews: 20120
size: Varies with device
website: https://www.coinomi.com
repository: 
issue: 
icon: com.coinomi.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-11-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinomiWallet
providerLinkedIn: coinomi/
providerFacebook: coinomi
providerReddit: COINOMI

redirect_from:
  - /coinomi/
  - /com.coinomi.wallet/
  - /posts/2019/11/coinomi/
  - /posts/com.coinomi.wallet/
---


This wallet
claims to be non-custodial but we cannot find any source code on their
[official GitHub page](https://github.com/coinomi/).

Our verdict: This app is **not verifiable**.
