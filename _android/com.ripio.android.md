---
wsId: 
title: "Ripio Bitcoin Wallet: the new digital economy"
altTitle: 
authors:
- leo
users: 1000000
appId: com.ripio.android
launchDate: 2015-06-01
latestUpdate: 2021-04-26
apkVersionName: "4.10.6"
stars: 3.7
ratings: 17301
reviews: 8811
size: 45M
website: https://www.ripio.com/ar/wallet
repository: 
issue: 
icon: com.ripio.android.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-03-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ripioapp
providerLinkedIn: ripio
providerFacebook: RipioApp
providerReddit: 

redirect_from:
  - /ripio/
  - /com.ripio.android/
  - /posts/2019/11/ripio/
  - /posts/com.ripio.android/
---


Ripio Bitcoin Wallet: the new digital economy
does not claim to be non-custodial and looks much like an interface for an
exchange. Neither can we find any source code.

Therefore: This wallet is **not verifiable**.
