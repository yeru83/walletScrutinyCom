---
wsId: 
title: "EBC Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: com.ebcecosystem.wallets
launchDate: 2018-12-07
latestUpdate: 2019-01-30
apkVersionName: "0.0.7"
stars: 4.7
ratings: 96
reviews: 60
size: 11M
website: https://ebc.eco
repository: 
issue: 
icon: com.ebcecosystem.wallets.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-28
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.ebcecosystem.wallets/
  - /posts/com.ebcecosystem.wallets/
---


The description of this app talks about how many coins it supports and mentions
of KYC and give the impression it is a custodial service. Their website is
currently down:

Cloudflare says:
> Error 521 Ray ID: 54c2d0873f95d64d • 2019-12-28 10:22:41 UTC
  Web server is down

Absent further information we have to assume this is a custodial service.

Our verdict: **not verifiable**.
