---
wsId: Bexplus
title: "Bitcoin Wallet for Margin Trading - Bexplus App"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.lingxi.bexplus
launchDate: 
latestUpdate: 2020-11-09
apkVersionName: "2.0.5"
stars: 4.8
ratings: 4347
reviews: 1644
size: 12M
website: https://www.bexplus.com
repository: 
issue: 
icon: com.lingxi.bexplus.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BexplusExchange
providerLinkedIn: 
providerFacebook: 
providerReddit: Bexplus

redirect_from:

---


The Bexplus website states under the mobile wallet section "Assets Security"

> Assets are stored in cold storage against stealing and loss

this leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

