---
wsId: 
title: "Wealth Check - Bitcoin Wallet Balance and History"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.jurajkusnier.bitcoinwalletbalance
launchDate: 
latestUpdate: 2020-06-01
apkVersionName: "2.4"
stars: 4.0
ratings: 204
reviews: 114
size: 5.0M
website: https://jurajkusnier.com/
repository: https://github.com/jurajkusnier/bitcoin-balance-check/
issue: 
icon: com.jurajkusnier.bitcoinwalletbalance.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is only for retrival of bitcoin address balance and transaction information.

Our verdict: This is **not a wallet**.

