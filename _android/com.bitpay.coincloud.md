---
wsId: 
title: "Coin Cloud Wallet"
altTitle: 
authors:

users: 10000
appId: com.bitpay.coincloud
launchDate: 
latestUpdate: 2021-04-27
apkVersionName: "11.2.21"
stars: 3.7
ratings: 123
reviews: 57
size: 18M
website: 
repository: 
issue: 
icon: com.bitpay.coincloud.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-13
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


