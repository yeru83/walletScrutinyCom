---
wsId: xwallet
title: "XWallet"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.pundix.xwallet
launchDate: 
latestUpdate: 2020-11-23
apkVersionName: "2.8.2"
stars: 3.1
ratings: 4951
reviews: 2870
size: 62M
website: https://pundix.com
repository: 
issue: 
icon: com.pundix.xwallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: PundiXLabs
providerLinkedIn: pundix
providerFacebook: pundixlabs
providerReddit: 

redirect_from:

---


Searching the Pundix [support FAQ](https://support.pundix.com/) we find an FAQ that answers the private key managment question.

> **Will I have a private key when setting up an account in XWallet app?**<br>
  No. Your email address and mobile number are required when setting up an XWallet app account.

The wallet does not provide the user access to the private keys.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.

