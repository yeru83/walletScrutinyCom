---
wsId: 
title: "Dharma — Your Ethereum Wallet"
altTitle: 
authors:

users: 10000
appId: io.dharma.Dharma
launchDate: 
latestUpdate: 2021-03-25
apkVersionName: "1.0.22"
stars: 3.0
ratings: 240
reviews: 158
size: 56M
website: https://www.dharma.io
repository: 
issue: 
icon: io.dharma.Dharma.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


