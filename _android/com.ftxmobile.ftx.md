---
wsId: FTXPro
title: "FTX Pro"
altTitle: 
authors:
- leo
users: 100000
appId: com.ftxmobile.ftx
launchDate: 
latestUpdate: 2021-01-20
apkVersionName: "1.1.0"
stars: 4.9
ratings: 2618
reviews: 1419
size: 46M
website: https://ftx.com
repository: 
issue: 
icon: com.ftxmobile.ftx.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-08
reviewStale: false
signer: 
reviewArchive:


providerTwitter: FTX_Official
providerLinkedIn: 
providerFacebook: ftx.official
providerReddit: 

redirect_from:

---


On their description there is not much to be found about it being even a wallet
for Bitcoin but as you can deposit Bitcoins into your account and withdraw them,
it technically works like a wallet but

> FTX is a cryptocurrency derivatives exchange built by traders, for traders.

and that is most likely custodial. Absent contrary claims we file it as such and
assume it is **not verifiable**.
