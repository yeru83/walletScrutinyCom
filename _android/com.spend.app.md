---
wsId: spend
title: "Spend App"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.spend.app
launchDate: 
latestUpdate: 2020-07-15
apkVersionName: "3.09"
stars: 3.5
ratings: 416
reviews: 233
size: 31M
website: https://www.spend.com/
repository: 
issue: 
icon: com.spend.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Spendcom
providerLinkedIn: 
providerFacebook: spend
providerReddit: Spend

redirect_from:

---


No statements regarding private key managment can be found on the [providers website](https://www.spend.com/app) or [Support section](https://help.spend.com).
It would be prudent to assume the private keys are under the control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

