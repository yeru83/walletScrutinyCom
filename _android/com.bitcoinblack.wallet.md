---
wsId: bitcoinblack
title: "Bitcoin Black Wallet"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.bitcoinblack.wallet
launchDate: 
latestUpdate: 2020-12-25
apkVersionName: "1.0.6"
stars: 3.0
ratings: 1522
reviews: 922
size: 25M
website: https://bitcoin.black/
repository: 
issue: 
icon: com.bitcoinblack.wallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BCB_Official1
providerLinkedIn: 
providerFacebook: bitcoinblackofficial
providerReddit: AllAboardBitcoinBlack

redirect_from:

---


This is a wallet for an alt coin "Bitcoin Black"
