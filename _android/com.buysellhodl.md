---
title: "🔥🔥BuySellHodl: Crypto Predictions, Ratings, News"
altTitle: 

users: 10000
appId: com.buysellhodl
launchDate: 
latestUpdate: 2019-10-11
apkVersionName: "1.12.7"
stars: 4.3
ratings: 157
reviews: 71
size: 12M
website: http://www.buysellhodlapp.com
repository: 
issue: 
icon: com.buysellhodl.png
bugbounty: 
verdict: defunct # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, reproducible, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BuySellHodlApp
providerLinkedIn: 
providerFacebook: groups/buysellhodl
providerReddit: 

redirect_from:
  - /com.buysellhodl/
  - /posts/com.buysellhodl/
---


This app appears to only provide information without being a wallet.
