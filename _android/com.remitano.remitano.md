---
wsId: Remitano
title: "Remitano - Buy & Sell Bitcoin Fast & Securely"
altTitle: 
authors:
- leo
users: 500000
appId: com.remitano.remitano
launchDate: 
latestUpdate: 2021-04-27
apkVersionName: "5.30.0"
stars: 4.2
ratings: 11937
reviews: 5676
size: 37M
website: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:
  - /posts/com.remitano.remitano/
---


This app is an interface to an exchange which holds your coins. On Google Play
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.
