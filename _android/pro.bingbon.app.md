---
wsId: bingbon
title: "Bingbon Bitcoin & Cryptocurrency Platform"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: pro.bingbon.app
launchDate: 
latestUpdate: 2021-05-02
apkVersionName: "2.31.3"
stars: 4.1
ratings: 686
reviews: 437
size: 26M
website: https://bingbon.com
repository: 
issue: 
icon: pro.bingbon.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-21
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BingbonOfficial
providerLinkedIn: bingbon
providerFacebook: BingbonOfficial
providerReddit: Bingbon

redirect_from:

---


We cannot find any claims as to the custody of private keys found from Bingbon.
We must assume the wallet app is custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
