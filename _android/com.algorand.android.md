---
wsId: 
title: "Algorand Wallet"
altTitle: 
authors:

users: 50000
appId: com.algorand.android
launchDate: 
latestUpdate: 2021-04-16
apkVersionName: "4.8.0"
stars: 4.9
ratings: 1994
reviews: 654
size: 37M
website: 
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.algorand.android/
---


