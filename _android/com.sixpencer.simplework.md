---
wsId: dfox
title: "Dfox-Crypto Wallet and DeFi Portfolio"
altTitle: 
authors:
- leo
users: 5000
appId: com.sixpencer.simplework
launchDate: 
latestUpdate: 2021-04-07
apkVersionName: "1.3.7"
stars: 4.6
ratings: 120
reviews: 58
size: 18M
website: https://dfox.cc
repository: 
issue: 
icon: com.sixpencer.simplework.jpg
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app appears not to get access to spend your Bitcoins:

> Dfox is a chain-agnostic crypto portfolio tracker.
