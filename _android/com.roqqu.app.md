---
wsId: roqqu
title: "Roqqu: Buy & Sell Bitcoin and Cryptocurrency Fast"
altTitle: 
authors:
- kiwilamb
- leo
users: 100000
appId: com.roqqu.app
launchDate: 
latestUpdate: 2021-04-24
apkVersionName: "1.3.2"
stars: 3.4
ratings: 12011
reviews: 8592
size: 27M
website: https://roqqu.com
repository: 
issue: 
icon: com.roqqu.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: roqqupay
providerLinkedIn: 
providerFacebook: roqqupay
providerReddit: 

redirect_from:

---


The provider claims:

> SAFETY FIRST<br>
  Over 98% of cryptocurrency is stored securely offline and the rest is
  protected by industry-leading online security.

which means you do not control the keys to your Bitcoins.

It is somewhat obscure weather this wallet can even store bitcoins, but under
their FAQ section on their website it contains an article on how to
[send and receive bitcoins](https://roqqu.com/knowledge/articles/send/how-to-send-and-receive-btc)
with addresses and QR codes displayed.

Our verdict: This "wallet" is custodial and therefore **not verifiable**.
