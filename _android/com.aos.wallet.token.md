---
wsId: 
title: "ATS Wallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.aos.wallet.token
launchDate: 
latestUpdate: 2020-08-07
apkVersionName: "2.0.22"
stars: 5.0
ratings: 330
reviews: 148
size: 34M
website: https://atswallet.io/
repository: 
issue: 
icon: com.aos.wallet.token.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


A clear statement can be found in the [play store description](https://play.google.com/store/apps/details?id=com.aos.wallet.token) regarding the management of private keys.

> "Superb Security: Your private keys never leave your device. Strong wallet encryption and cryptography guarantee that your funds will stay safe under your ultimate control."

Sounds non-custodial, but we couldn't find any source code to look deeper. Also there are a number of warning signs about this wallet that seem very scammy ...

**Warning**
I would be cautious of this wallet, there seem to be anumber of warning signs
1. Alot of the links on the providers website for the social media do not link anywhere.
2. They have a Iphone app linked on their website but they do not seem to have an iphone version of the app.
3. The play store reviews are suspisious, 300+ reviews all 5 star and on the same date!

Our verdict: This 'wallet' claims to be non-custodial, however with no source code found this wallet is **not verifiable**.
