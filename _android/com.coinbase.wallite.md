---
wsId: CoinbaseWallet
title: "Coinbase Wallet Lite"
altTitle: 
authors:

users: 50000
appId: com.coinbase.wallite
launchDate: 
latestUpdate: 2020-07-17
apkVersionName: "0.5-alpha"
stars: 3.5
ratings: 244
reviews: 155
size: 6.7M
website: https://wallet.coinbase.com
repository: 
issue: 
icon: com.coinbase.wallite.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-19
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is provided by the same developer as
[this app](/android/com.coinbase.android), the
[provided website](https://wallet.coinbase.com/) links to
[this other app](/android/org.toshi) though. It's a bit confusing.

Reading the description it turns out, this app is for ETH only.
